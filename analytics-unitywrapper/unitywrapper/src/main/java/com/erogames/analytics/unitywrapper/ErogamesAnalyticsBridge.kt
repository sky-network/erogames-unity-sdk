package com.erogames.analytics.unitywrapper

import androidx.annotation.Keep
import com.erogames.analytics.ErogamesAnalytics
import com.unity3d.player.UnityPlayer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

@Keep
object ErogamesAnalyticsBridge {

    @JvmStatic
    fun init(clientId: String) {
        UnityPlayer.currentActivity?.let {
            ErogamesAnalytics.init(it, clientId)
        }
    }

    @JvmOverloads
    @JvmStatic
    fun logEvent(event: String, paramsJson: String? = null) {
        UnityPlayer.currentActivity?.let {
            val params =
                Json { ignoreUnknownKeys = true }.decodeFromString<Map<String, String>>(
                    paramsJson
                        ?: "{}"
                )
            ErogamesAnalytics.logEvent(it, event, params)
        }
    }
}