package com.erogames.analytics.unitywrapper.sample

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.erogames.analytics.unitywrapper.ErogamesAnalyticsBridge

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ErogamesAnalyticsBridge.init("some_client_id")
        findViewById<Button>(R.id.log_event).setOnClickListener {
            val event = "install"
            val target = "eroges-app"
            val paramsStr = "{\"target\":\"$target\"}"
            ErogamesAnalyticsBridge.logEvent(event, paramsStr)
        }
    }
}