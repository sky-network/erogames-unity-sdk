using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAnalyticsNS
{
    public class ErogamesAnalytics
    {
        private ErogamesAnalytics() { }

        public static void Init(string clientId)
        {
            Init(clientId, null);
        }

        public static void Init(string clientId, string whitelabelId)
        {
            if (string.IsNullOrEmpty(whitelabelId)) whitelabelId = Constants.WhitelabelId;
            if (Application.platform == RuntimePlatform.Android)
                ErogamesAnalyticsAndroidBridge.Instance.Init(clientId);
            else
                ErogamesAnalyticsBridge.Instance.Init(clientId, whitelabelId);
        }

        /// <summary>
        /// Send an event to the server asynchronously.
        /// The event will be added to the queue immediately.
        /// But sending may be delayed if, for example, there is no internet connection, etc.
        /// A user does not have to worry about it because the event will be stored until it is sent.
        ///
        /// See also: https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/analytics
        /// </summary>
        /// <param name="eroEvent">Event name</param>
        public static void LogEvent(string eventId)
        {
            LogEvent(eventId, new Dictionary<string, string>());
        }

        /// <summary>
        /// Send an event to the server asynchronously.
        /// The event will be added to the queue immediately.
        /// But sending may be delayed if, for example, there is no internet connection, etc.
        /// A user does not have to worry about it because the event will be stored until it is sent.
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="data"></param>
        public static void LogEvent(string eventId, Dictionary<string, string> data)
        {
            if (Application.platform == RuntimePlatform.Android)
                ErogamesAnalyticsAndroidBridge.Instance.LogEvent(eventId, data);
            else
                ErogamesAnalyticsBridge.Instance.LogEvent(eventId, data);
        }
    }
}
