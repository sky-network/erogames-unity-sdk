using System;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Util;
using UnityEngine;

namespace ErogamesAnalyticsNS
{
    internal sealed class ErogamesAnalyticsAndroidBridge : Singleton<ErogamesAnalyticsAndroidBridge>
    {
        private const string BridgeClassName = "com.erogames.analytics.unitywrapper.ErogamesAnalyticsBridge";
        private AndroidJavaClass bridgeJavaClass;

        private AndroidJavaClass GetBridgeJavaClass()
        {
            if (bridgeJavaClass == null)
            {
                bridgeJavaClass = new AndroidJavaClass(BridgeClassName);
            }
            return bridgeJavaClass;
        }

        internal void Init(string clientId)
        {
            GetBridgeJavaClass().CallStatic("init", clientId);
        }

        public void LogEvent(string eroEvent, Dictionary<string, string> eroParams)
        {
            GetBridgeJavaClass().CallStatic("logEvent", eroEvent, Utils.DictionaryToJson(eroParams));
        }
    }
}
