﻿using System;
using System.Collections.Generic;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class ErrorResp
    {
        public string message;
        public List<string> errors;
        public List<string> messages;
        public string error_description;
    }
}
