# Unity Erogames Analytics Plugin

## Requirements
Unity version `5.6.4` or higher.

## Get started

### How to install
1. Add the following line as dependency to Packages/manifest.json:
```json
"com.erogames.analytics": "https://gitlab.com/sky-network/erogames-unity-sdk.git#analytics-v<x.y.z>-upm"
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-unity-sdk/-/tags).  
If the tag name is `analytics-v1.2.0-upm` then `<x.y.z>` is `1.2.0`.

2. Download and import [External Dependency Manager for Unity (EDM4U)](https://github.com/googlesamples/unity-jar-resolver/raw/v1.2.174/external-dependency-manager-1.2.174.unitypackage) into a project.
3. [Optionally] In case Unity Package Manager is unavailable just import `erogames-analytics-v<x.y.z>.unitypackage` into a project.
4. Go to (in Unity Editor) `Assets > External Dependency Manager > Android Resolver` and click `Resolve` (in some cases `Force Resolve`).

### How to make Android build on Unity with Gradle version less than 5.1.1 (see [Gradle for Android](https://docs.unity3d.com/Manual/android-gradle-overview.html))
1. Export project (`File > Build Settings > Export Project`).
2. Open the project in Android Studio.
3. Update Android Gradle Plugin version to 3.4.0 or higher. In the project-level `build.gradle`:
```groovy
dependencies {
  ...
  classpath 'com.android.tools.build:gradle:3.4.0' // or higher
  ...
}
```
4. Add Java 8 language features compatibility. In the project-level `build.gradle` (or in the app-level `build.gradle` if exists):
```groovy
android {
  ...
  compileOptions {
      sourceCompatibility JavaVersion.VERSION_1_8
      targetCompatibility JavaVersion.VERSION_1_8
  }
  ...
}
```

### Initialize before use
```csharp
ErogamesAnalytics.Init("your_client_id");
```

### Log event
To override data used by ErogamesAnalytics internally, add `ea_` prefix to the param name.  
To add custom data, the param name should not be started with `ea_` prefix.

```csharp
string event = "install";
ErogamesAnalytics.LogEvent(event);
```
Log event with a payload:
```csharp
string event = "click";
Dictionary<string, string> data = new Dictionary<string, string>()
{
  ["ea_category"] = "banners"
};
ErogamesAnalytics.LogEvent(event, data);
```

### How to debug on Android:
```shell
adb logcat -s EventWorker -s TrackProviderUtil
```
