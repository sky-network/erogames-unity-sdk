# Unity Erogames Updater Plugin

## Requirements
* Unity version `2018.4` or higher.
* Minimum supported Android API level 23.

## Install

1. Add the following line as dependency to Packages/manifest.json:
```json
"com.erogames.updater": "https://gitlab.com/sky-network/erogames-unity-sdk.git#updater-v<x.y.z>-upm"
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-unity-sdk/-/tags).  
If the tag name is `updater-v1.0.0-upm` then `<x.y.z>` is `1.0.0`.

2. Download and import [External Dependency Manager for Unity (EDM4U)](https://github.com/googlesamples/unity-jar-resolver/raw/v1.2.174/external-dependency-manager-1.2.174.unitypackage) into a project.
3. [Optionally] In case Unity Package Manager is unavailable, just import `erogames-updater-v<x.y.z>.unitypackage` into a project.
4. Go to (in Unity Editor) `Assets > External Dependency Manager > Android Resolver` and click `Resolve` (in some cases `Force Resolve`).

## Usage

### Init:
```csharp
ErogamesUpdater.Init(accessKey, whitelabelId, locale);
```

### Update:
```csharp
ErogamesUpdater.Update(listener);
```

### Stop:
```csharp
ErogamesUpdater.Stop();
```
