using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using ErogamesAuthNS.Api;
using ErogamesAuthNS.Entity;
using ErogamesAuthNS.Repository;
using ErogamesAuthNS.Util;
using ErogamesUpdaterNS.Entity;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace ErogamesUpdaterNS
{
    internal class DesktopUpdater : Singleton<DesktopUpdater>
    {
        private BaseRepository _repository;
        private static IUpdateListener _updateListener;
        private static readonly string Uuid = Guid.NewGuid().ToString();
        private readonly string _updSuffix = $".{Uuid}.upd";
        private readonly string _oldSuffix = $".{Uuid}.old";

        internal void Init(string appId, string wlId, string accessKey, string locale)
        {
            _repository = new BaseRepository(new ApiService());
            _repository.SetConfig(new WlConfig(appId, wlId, accessKey, locale));
        }

        internal IEnumerator StartUpdate(IUpdateListener updateListener)
        {
            _updateListener = updateListener;
            _updateListener?.OnResult(new UpdateState(State.Running));

            try
            {
                Directory.GetFileSystemEntries(GetGamePath());
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                _updateListener?.OnResult(new UpdateState(State.Failure, 0, e.Message));
                yield break;
            }

            // Fetch game data
            var gameItemResult = Result<GameItem>.Undefined();
            yield return _repository.GetGameItem(onResult => { gameItemResult = onResult; });
            var gameItem = gameItemResult.Data;
            if (gameItemResult is Result<GameItem>.ErrorResult)
            {
                _updateListener?.OnResult(new UpdateState(State.Failure, 0, gameItemResult.ErrorMsg));
                yield break;
            }

            var downloadDirPath = GetDownloadDirPath(_repository.GetWhitelabelId(), gameItem.slug);
            if (Directory.Exists(downloadDirPath)) Directory.Delete(downloadDirPath, true);
            Directory.CreateDirectory(downloadDirPath);
            Debug.Log($"Download directory: {downloadDirPath}");

            // Download game
            var downloadResult = Result<string>.Undefined();
            yield return Download(
                gameItem,
                gameItem.GetPatch(Application.version),
                GetGamePath(),
                downloadDirPath,
                progress => _updateListener?.OnResult(new UpdateState(State.Running, progress)),
                result => downloadResult = result
            );

            if (downloadResult is Result<string>.ErrorResult)
            {
                _updateListener?.OnResult(new UpdateState(State.Failure, 0, downloadResult.ErrorMsg));
                yield break;
            }

            Complete(downloadDirPath, downloadResult.Data);
        }

        private void Complete(string downloadDirPath, string downloadGamePath)
        {
            var gameExec = GetGameExec(GetGamePath());
            var downloadGameExec = GetDownloadGameExec(downloadGamePath);
            var newGameExec = Utils.PathCombine(GetGamePath(), Path.GetFileName(downloadGameExec));
            Debug.Log($"Game executable: {gameExec}");
            Debug.Log($"Download game executable: {downloadGameExec}");
            Debug.Log($"New game executable: {newGameExec}");

            // Add '_updSuffix' to download/unzipped entries
            var updPaths = new Dictionary<string, string>();
            foreach (var fileEntry in Directory.GetFileSystemEntries(downloadGamePath))
            {
                var newEntryName = $"{fileEntry}{_updSuffix}";
                if (Directory.Exists(fileEntry)) Directory.Move(fileEntry, newEntryName);
                if (File.Exists(fileEntry)) File.Move(fileEntry, newEntryName);
            }

            // Copy download/unzipped entries to game directory
            Utils.Copy(downloadGamePath, GetGamePath());
            // Delete download directory
            Directory.Delete(downloadDirPath, true);

            foreach (var fileEntry in Directory.GetFileSystemEntries(GetGamePath())
                         .Where(entry => entry.EndsWith(_updSuffix)))
            {
                var newEntryPath = fileEntry.Replace($"{_updSuffix}", "");
                updPaths.Add(fileEntry, newEntryPath);
            }

            // Add '_oldSuffix' to game directory entries
            var oldPaths = GetPathsToRemove(GetGamePath(), gameExec);

            _updateListener?.OnResult(new UpdateState(State.Done, 100));
            // Launch game and remove '_oldSuffix' game entries
            LaunchGame(GetGamePath(), newGameExec, updPaths, oldPaths);
            Application.Quit();
        }

        private string GetGamePath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        private IEnumerator Download(
            GameItem gameItem,
            Patch patch,
            string gamePath,
            string downloadDirPath,
            Action<int> progress,
            Action<Result<string>> onResult)
        {
            var patchOrFull = patch != null ? "patch" : "full file";
            Debug.Log($"Download {patchOrFull}...");

            var downloadFilePath = Utils.PathCombine(downloadDirPath, $"{gameItem.slug}.dwld");
            if (File.Exists(downloadFilePath)) File.Delete(downloadFilePath);

            var downloadUrl = patch?.url ?? gameItem.download_url;
            var downloadResult = Result<bool>.Undefined();
            yield return _repository.Download(
                downloadUrl,
                downloadFilePath,
                progress,
                result => downloadResult = result
            );

            if (downloadResult is Result<bool>.ErrorResult)
            {
                onResult(Result<string>.Error(downloadResult.ErrorMsg));
                yield break;
            }

            // Unzip downloaded file
            if (patch == null)
            {
                var unzipPath = Utils.PathCombine(downloadDirPath, "unzip");
                if (Directory.Exists(unzipPath)) Directory.Delete(unzipPath, true);
                Directory.CreateDirectory(unzipPath);

                var unzipResult = Utils.Unzip(downloadFilePath, unzipPath);
                if (!unzipResult)
                {
                    onResult(Result<string>.Error("Unzip: something went wrong."));
                    yield break;
                }

                // Delete __MACOSX directory
                var macOsxPath = Utils.PathCombine(unzipPath, "__MACOSX");
                if (Directory.Exists(macOsxPath)) Directory.Delete(macOsxPath, true);

                var gameExecutable = GetDownloadGameExec(unzipPath);
                if (Utils.IsMac()) FixMacOSPermission(gameExecutable);
                onResult(Result<string>.Success(unzipPath));
            }
            else
            {
                var patchedPath = Utils.PathCombine(downloadDirPath, "patched");
                if (Directory.Exists(patchedPath)) Directory.Delete(patchedPath, true);
                var outPath = patchedPath;
                if (Utils.IsMac())
                {
                    var gameExecName = Path.GetFileName(GetGameExec(gamePath));
                    Directory.CreateDirectory(patchedPath);
                    outPath = Utils.PathCombine(patchedPath, gameExecName);
                }

                Debug.Log("Applying patch...");
                var source = Utils.IsMac() ? GetGameExec(gamePath) : gamePath;
                if (!Patcher.Patch(source, downloadFilePath, outPath))
                {
                    Debug.Log("Applying patch failed. Download full version...");
                    yield return Download(
                        gameItem,
                        null,
                        gamePath,
                        downloadDirPath,
                        progress,
                        onResult
                    );
                }
                else
                {
                    onResult(Result<string>.Success(patchedPath));
                }
            }
        }

        private Dictionary<string, string> GetPathsToRemove(string origGamePath, string origExecutable)
        {
            return (Utils.IsMac())
                ? MacPathsToRemove(origExecutable)
                : WinPathsToRemove(origGamePath, origExecutable);
        }

        private Dictionary<string, string> MacPathsToRemove(string origExecutable)
        {
            var executableOrigPath = $"{origExecutable}{_oldSuffix}";
            if (Directory.Exists(executableOrigPath)) Directory.Delete(executableOrigPath, true);
            return new Dictionary<string, string> { { origExecutable, executableOrigPath } };
        }

        private Dictionary<string, string> WinPathsToRemove(string origGamePath, string origExecutable)
        {
            var pathToRemove = new Dictionary<string, string>();
            // base .exe
            var origExecutableOld = $"{origExecutable}{_oldSuffix}";
            if (File.Exists(origExecutableOld)) File.Delete(origExecutableOld);
            pathToRemove.Add(origExecutable, origExecutableOld);

            // MonoBleedingEdge
            var monoPath = Utils.PathCombine(origGamePath, "MonoBleedingEdge");
            var monoPathOld = $"{monoPath}{_oldSuffix}";
            if (Directory.Exists(monoPathOld)) Directory.Delete(monoPathOld, true);
            pathToRemove.Add(monoPath, monoPathOld);

            // UnityPlayer.dll
            var playerPath = Utils.PathCombine(origGamePath, "UnityPlayer.dll");
            var playerPathOld = $"{playerPath}{_oldSuffix}";
            if (File.Exists(playerPathOld)) File.Delete(playerPathOld);
            pathToRemove.Add(playerPath, playerPathOld);

            // UnityCrashHandler32.exe
            var handler32Path = Utils.PathCombine(origGamePath, "UnityCrashHandler32.exe");
            var handler32PathOld = $"{handler32Path}{_oldSuffix}";
            if (File.Exists(handler32PathOld)) File.Delete(handler32PathOld);
            pathToRemove.Add(handler32Path, handler32PathOld);

            // UnityCrashHandler64.exe
            var handler64Path = Utils.PathCombine(origGamePath, "UnityCrashHandler64.exe");
            var handler64PathOld = $"{handler64Path}{_oldSuffix}";
            if (File.Exists(handler64PathOld)) File.Delete(handler64PathOld);
            pathToRemove.Add(handler64Path, handler64PathOld);

            // _Data 
            var dataDirName = $"{Path.GetFileNameWithoutExtension(origExecutable)}_Data";
            var dataPath = Utils.PathCombine(origGamePath, dataDirName);
            var dataPathOld = $"{dataPath}{_oldSuffix}";
            if (Directory.Exists(dataPathOld)) Directory.Delete(dataPathOld, true);
            pathToRemove.Add(dataPath, dataPathOld);
            return pathToRemove;
        }

        private string GetGameExec(string origGamePath)
        {
            return Utils.IsMac()
                ? Utils.PathCombine(origGamePath, Directory.GetParent(Application.dataPath)!.Name)
                : Utils.PathCombine(origGamePath,
                    $"{Process.GetCurrentProcess().MainModule!.FileName}");
        }

        private void LaunchGame(string gamePath,
            string newOrigExecutable,
            Dictionary<string, string> updPaths,
            Dictionary<string, string> oldPath,
            int sleepSec = 2
        )
        {
            if (Utils.IsMac())
                MacLaunchGame(gamePath, newOrigExecutable, updPaths, oldPath, sleepSec);
            else
                WinLaunchGame(gamePath, newOrigExecutable, updPaths, oldPath, sleepSec);
        }

        private void MacLaunchGame(string gamePath,
            string newOrigExecutable,
            Dictionary<string, string> updPaths,
            Dictionary<string, string> oldPath,
            int sleepSec = 2
        )
        {
            var args = new StringBuilder();
            args.Append("-c ");
            args.Append(@"""");
            args.Append($"ping -t {sleepSec} 127.0.0.1 > /dev/null & ");

            // mark as old
            foreach (var (key, value) in oldPath) args.Append($"mv \\\"{key}\\\" \\\"{value}\\\" && ");

            // mark upd as new
            foreach (var (key, value) in updPaths) args.Append($"mv \\\"{key}\\\" \\\"{value}\\\" && ");

            args.Append($"open \\\"{newOrigExecutable}\\\" && ");
            args.Append($"find \\\"{gamePath}\\\" -type f -name \\\"*{_oldSuffix}\\\" -delete && ");
            args.Append($"find \\\"{gamePath}\\\" -type d -name \\\"**{_oldSuffix}\\\" -exec rm -r {{}} +");
            args.Append(@"""");

            Utils.RunHiddenCommand("bash", $"{args}");
        }

        private void WinLaunchGame(string gamePath,
            string newOrigExecutable,
            Dictionary<string, string> updPaths,
            Dictionary<string, string> oldPath,
            int sleepSec = 2
        )
        {
            var args = new StringBuilder();
            args.Append("/K ");
            args.Append(@"""");
            args.Append($"ping -n {sleepSec} 127.0.0.1 > nul & ");

            // mark as old
            foreach (var (key, value) in oldPath) args.Append($@"move ""{key}"" ""{value}"" & ");
            // mark upd as new
            foreach (var (key, value) in updPaths) args.Append($@"move ""{key}"" ""{value}"" & ");

            args.Append($@"start """" ""{newOrigExecutable}"" & ");
            var markedAsOld = Utils.PathCombine(gamePath, $"*{_oldSuffix}").Replace(@"\\", @"\");
            args.Append($@"del /s ""{markedAsOld}"" & ");
            args.Append($@"for /d %d in (""{markedAsOld}"") do rmdir /s /q ""%d""");
            args.Append(@"""");

            Utils.RunHiddenCommand("cmd.exe", $"{args}");
        }

        private static void FixMacOSPermission(string gameExecutable)
        {
            var plistPath = Utils.PathCombine(gameExecutable, "Contents", "Info.plist");
            var appName = Utils.GetPlistValue(plistPath, "CFBundleName");
            var binPath = Utils.PathCombine(gameExecutable, "Contents", "MacOS", appName);

            const string command = "chmod";
            var arguments = $"+x \"{binPath}\"";
            Utils.RunCommand(command, arguments, result =>
            {
                Debug.Log($"_command_result: {result.Data}");
                Debug.Log($"_command_result_error: {result.ErrorMsg}");
            });
        }

        private string GetDownloadGameExec(string destination)
        {
            return Utils.IsMac()
                ? GetMacGameExecutable(destination)
                : GetWinGameExecutable(destination);
        }

        private static string GetMacGameExecutable(string gamePath)
        {
            return Directory.GetFileSystemEntries(gamePath).First(file => file.EndsWith(".app"));
        }

        private static string GetWinGameExecutable(string gamePath)
        {
            return Directory.GetFiles(gamePath)
                .First(file => file.EndsWith(".exe") && !file.Contains("UnityCrashHandler"));
        }

        private static string GetDownloadDirPath(string wlId, string slug)
        {
            return Utils.PathCombine(
                Application.temporaryCachePath,
                $"{wlId}_updtr"
            );
        }

        internal void Stop()
        {
            _updateListener?.OnResult(new UpdateState());
        }
    }
}