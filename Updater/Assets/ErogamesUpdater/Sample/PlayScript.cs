using System;
using ErogamesUpdaterNS.Entity;
using UnityEngine;
using UnityEngine.UI;

namespace ErogamesUpdaterNS.Sample
{
    public class PlayScript : MonoBehaviour, IUpdateListener
    {
        public GameObject mainCamera;
        public Button updateButton;
        public Button stopButton;
        public Text textView;

        private void Awake()
        {
            const string accessKey = "<accessKey>";
            const string whitelabelId = "erogames";
            const string locale = "en";
            ErogamesUpdater.Init(accessKey, whitelabelId, "<app_id>", locale);
        }

        public void OnUpdate()
        {
            textView.text = "Start";
            ErogamesUpdater.Update(this);
        }

        public void OnStop() => ErogamesUpdater.Stop();

        public void OnResult(UpdateState updateState)
        {
            textView.text =
                $"state: {updateState.state}, progress: {updateState.progress}, error: {updateState.message}";
            switch (updateState.state)
            {
                case State.Idle:
                    break;
                case State.Running:
                    Debug.Log("Update progress: " + updateState.progress);
                    break;
                case State.Done:
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        Debug.Log("An update dialog (or notification) was shown to the user.");
                    }
                    else
                    {
                        Debug.Log("The game is restarting...");
                    }

                    break;
                case State.Failure:
                    Debug.LogError("updateState error: " + updateState.message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}