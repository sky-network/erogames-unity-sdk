﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ErogamesAuthNS.Api;
using ErogamesAuthNS.Entity;
using ErogamesAuthNS.Util;

namespace ErogamesAuthNS.Repository
{
    internal class BaseRepository
    {
        private readonly ApiService _apiService;
        private WlConfig _wlConfig;

        internal BaseRepository(ApiService apiService)
        {
            _apiService = apiService;
        }

        internal void SetConfig(WlConfig wlConfig)
        {
            _wlConfig = wlConfig;
        }

        private WlConfig GetConfig()
        {
            if (_wlConfig == null) throw new Exception("WLConfig == null");
            return _wlConfig;
        }

        internal string GetWhitelabelId()
        {
            return GetConfig().WlId;
        }


        private IEnumerator GetWhitelabel(string jwtToken, string wlId, Action<Result<Whitelabel>> onResult)
        {
            var wlResult = Result<List<Whitelabel>>.Undefined();
            yield return _apiService.GetWhitelabels(jwtToken, result => wlResult = result);

            var whitelabel = wlResult.Data.FirstOrDefault(whitelabel => whitelabel.slug == wlId);
            if (wlResult is Result<List<Whitelabel>>.SuccessResult && whitelabel == null)
            {
                onResult(Result<Whitelabel>.Error($"Whitelabel ({wlId}) does not exist."));
                yield break;
            }

            onResult(wlResult is Result<List<Whitelabel>>.SuccessResult
                ? Result<Whitelabel>.Success(whitelabel)
                : Result<Whitelabel>.Error(wlResult.ErrorMsg));
        }

        internal IEnumerator GetGameItem(Action<Result<GameItem>> onResult)
        {
            var appId = GetConfig().AppId;
            var accessKey = GetConfig().AccessKey;
            var whitelabelId = GetConfig().WlId;
            var language = GetConfig().Locale;
            var qParams = new Dictionary<string, string>
            {
                { "locale", language },
                { "platform", Utils.GetPlatformName() }
            };

            var jwtTokenResult = Result<string>.Undefined();
            yield return _apiService.GetJwtToken(accessKey, respResult => jwtTokenResult = respResult);
            if (jwtTokenResult is Result<string>.ErrorResult)
            {
                onResult(Result<GameItem>.Error(jwtTokenResult.ErrorMsg));
                yield break;
            }

            var wlResult = Result<Whitelabel>.Undefined();
            yield return GetWhitelabel(jwtTokenResult.Data, whitelabelId, respResult => wlResult = respResult);
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<GameItem>.Error(wlResult.ErrorMsg));
                yield break;
            }

            yield return GetGameItemById(wlResult.Data.url, jwtTokenResult.Data, appId, qParams, respResult =>
            {
                onResult(respResult is Result<GameItem>.SuccessResult
                    ? Result<GameItem>.Success(respResult.Data)
                    : Result<GameItem>.Error(respResult.ErrorMsg));
            });
        }

        private IEnumerator GetGameItemById(string baseUrl, string jwtToken, string appId,
            Dictionary<string, string> qParams, Action<Result<GameItem>> onResult)
        {
            var gameItemResult = Result<List<GameItem>>.Undefined();
            yield return _apiService.GetGameItems(baseUrl, jwtToken, qParams, result => gameItemResult = result);
            var gameItem = gameItemResult.Data.FirstOrDefault(gameItem =>
                !string.IsNullOrEmpty(gameItem.app_id) && gameItem.app_id == appId);
            if (gameItemResult is Result<List<GameItem>>.SuccessResult && gameItem == null)
            {
                onResult(Result<GameItem>.Error($"Game item ({appId}) does not exist."));
                yield break;
            }

            onResult(gameItemResult is Result<List<GameItem>>.SuccessResult
                ? Result<GameItem>.Success(gameItem)
                : Result<GameItem>.Error(gameItemResult.ErrorMsg));
        }

        internal IEnumerator Download(string url, string outputFile, Action<int> progress,
            Action<Result<bool>> onResult)
        {
            return _apiService.Download(url, outputFile, progress, onResult);
        }
    }
}