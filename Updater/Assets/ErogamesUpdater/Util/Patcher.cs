﻿using System;
using System.Globalization;
using ErogamesAuthNS.Entity;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal static class Patcher
    {
        internal static bool Patch(string sourcePath, string patchPath, string outPath)
        {
            var command = Utils.PathCombine(Application.streamingAssetsPath, GetAssetName());
            var result = Result<string>.Undefined();
            var args = $@"""{sourcePath}"" ""{patchPath}"" ""{outPath}""";
            Utils.RunCommand(command, args, onResult => result = onResult);
            if (result is Result<string>.ErrorResult) Debug.LogError(result.ErrorMsg);

            return result is Result<string>.SuccessResult;
        }

        private static string GetAssetName()
        {
            var os = Utils.GetPlatformName();
            string type;
            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(SystemInfo.processorType, "ARM",
                    CompareOptions.IgnoreCase) >= 0)
            {
                type = Environment.Is64BitProcess ? "_arm64" : "_arm";
            }
            else
            {
                type = Environment.Is64BitProcess ? "_64" : "";
            }

            if (Utils.IsMac()) type = "";
            var ext = Utils.IsMac() ? "" : ".exe";

            return $"{os}{type}_hpatchz{ext}";
        }
    }
}