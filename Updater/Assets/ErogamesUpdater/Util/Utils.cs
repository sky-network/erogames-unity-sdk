﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using UnityEngine;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using ErogamesAuthNS.Entity;
using Debug = UnityEngine.Debug;

namespace ErogamesAuthNS.Util
{
    internal static class Utils
    {
        internal static IEnumerator DoWork(Action action, Action<string> onError, BackgroundWorker worker = null)
        {
            var worker1 = worker ?? new BackgroundWorker();
            var completed = false;

            DoWorkEventHandler doWorkHandler = (_, _) =>
            {
                Debug.Log("DoWork: " + worker1.GetHashCode());
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    onError(e.Message);
                }

                completed = true;
            };

            worker1.DoWork += doWorkHandler;
            worker1.RunWorkerAsync();
            yield return new WaitUntil(() =>
            {
                if (!worker1.CancellationPending) return completed;
                onError("Cancelled");
                return true;
            });

            worker1.DoWork -= doWorkHandler;
        }

        internal static string GetPlatformName()
        {
            return Application.platform switch
            {
                RuntimePlatform.Android => "android",
                RuntimePlatform.OSXPlayer => "osx",
                RuntimePlatform.WindowsPlayer => "windows",
                RuntimePlatform.LinuxPlayer => "linux",
                RuntimePlatform.WebGLPlayer => "web",
                _ => ""
            };
        }

        internal static bool Unzip(string sourcePath, string destinationPath)
        {
            try
            {
                ZipFile.ExtractToDirectory(sourcePath, destinationPath);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError($"Error extracting archive: {e.Message}");
            }

            return false;
        }

        internal static bool IsMac()
        {
            return Application.platform == RuntimePlatform.OSXPlayer;
        }

        private static bool IsWin()
        {
            return Application.platform == RuntimePlatform.WindowsPlayer;
        }

        internal static string GetPlistValue(string plistPath, string plistKey)
        {
            var plistContent = File.ReadAllText(plistPath);
            const string regex = @"<key>(.*?)</key>\s*<.*?>(.*?)</.*?>";
            var matches = Regex.Matches(plistContent, regex);
            var result = new Dictionary<string, object>();

            foreach (Match match in matches)
            {
                var key = match.Groups[1].Value.Trim();
                var value = match.Groups[2].Value.Trim();
                result.Add(key, value);
                result[key] = value;
            }

            return result[plistKey] as string;
        }

        internal static void RunCommand(string command, string arguments, Action<Result<string>> onResult = null)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = command,
                Arguments = arguments,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            try
            {
                var process = new Process();
                process.StartInfo = startInfo;
                process.Start();
                var output = process.StandardOutput.ReadToEnd();
                var error = process.StandardError.ReadToEnd();
                process.WaitForExit();
                onResult?.Invoke(!string.IsNullOrEmpty(error)
                    ? Result<string>.Error(error)
                    : Result<string>.Success(output));
            }
            catch (Exception e)
            {
                onResult?.Invoke(Result<string>.Error(e.Message));
            }
        }

        internal static void RunHiddenCommand(string command, string arguments)
        {
            Debug.Log($"Command: {command}");
            Debug.Log($"Args: {arguments}");

            var proc = new Process();
            proc.StartInfo.FileName = command;
            proc.StartInfo.Arguments = arguments;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
        }

        internal static void Copy(string source, string destination)
        {
            var files = Directory.GetFiles(source, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var relativePath = file.Substring(source.Length + 1);
                var destinationFile = Path.Combine(destination, relativePath);
                Directory.CreateDirectory(Path.GetDirectoryName(destinationFile)!);
                File.Copy(file, destinationFile, true);
            }
        }

        internal static string PathCombine(params string[] paths)
        {
            var path = Path.GetFullPath(Path.Combine(paths));
            return IsWin() ? path.Replace(@"\", @"\\") : path;
        }
    }
}