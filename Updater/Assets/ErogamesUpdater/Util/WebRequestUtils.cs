﻿using ErogamesAuthNS.Entity;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAuthNS.Util
{
    internal static class WebRequestUtils
    {
        internal static bool IsSuccess(UnityWebRequest unityWebRequest)
        {
#if UNITY_2020_1_OR_NEWER
            return unityWebRequest.result == UnityWebRequest.Result.Success;
#else
            return !unityWebRequest.isNetworkError && !unityWebRequest.isHttpError;
#endif
        }

        internal static string GetError(UnityWebRequest unityWebRequest)
        {
            var rawResponse = unityWebRequest.downloadHandler.text;
            var errorResp = JsonUtility.FromJson<ErrorResp>(rawResponse);

            if (errorResp == null) return rawResponse;
            if (!string.IsNullOrEmpty(errorResp.message)) return errorResp.message;
            if (errorResp.errors?.Count > 0) return string.Join(", ", errorResp.errors.ToArray());
            return !string.IsNullOrEmpty(errorResp.error_description) ? errorResp.error_description : rawResponse;
        }
    }
}