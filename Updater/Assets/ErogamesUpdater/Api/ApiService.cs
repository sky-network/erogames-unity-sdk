﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using ErogamesAuthNS.Entity;
using ErogamesAuthNS.Util;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAuthNS.Api
{
    internal class ApiService
    {
        private const string SdkVersion = "1.1";
        private const string BaseURL = "https://erogames.com/";
        private const string JwtTokenEndPoint = "api/v1/authenticate";
        private const string WhitelabelsEndPoint = "api/v1/whitelabels";
        private const string GameItemsEndpoint = "/api/v1/items";

        internal IEnumerator GetJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            const string url = BaseURL + JwtTokenEndPoint;
            var form = new WWWForm();
            form.AddField("access_key", accessKey);

            using var unityWebRequest = UnityWebRequest.Post(url, form);
            unityWebRequest.SetRequestHeader("X-SDK-VERSION", SdkVersion);
            yield return unityWebRequest.SendWebRequest();

            onResult.Invoke(WebRequestUtils.IsSuccess(unityWebRequest)
                ? Result<string>.Success(JsonUtility.FromJson<JwtTokenResp>(unityWebRequest.downloadHandler.text).token)
                : Result<string>.Error(WebRequestUtils.GetError(unityWebRequest)));
        }

        internal IEnumerator GetWhitelabels(string jwtToken, Action<Result<List<Whitelabel>>> onResult)
        {
            const string url = BaseURL + WhitelabelsEndPoint;

            using var unityWebRequest = UnityWebRequest.Get(url);
            unityWebRequest.SetRequestHeader("Authorization", jwtToken);
            unityWebRequest.SetRequestHeader("X-SDK-VERSION", SdkVersion);
            yield return unityWebRequest.SendWebRequest();

            onResult.Invoke(WebRequestUtils.IsSuccess(unityWebRequest)
                ? Result<List<Whitelabel>>.Success(WhitelabelsResp.ToWhitelabels(unityWebRequest.downloadHandler.text))
                : Result<List<Whitelabel>>.Error(WebRequestUtils.GetError(unityWebRequest)));
        }

        internal IEnumerator GetGameItems(string baseUrl, string jwtToken, Dictionary<string, string> qParams,
            Action<Result<List<GameItem>>> onResult)
        {
            var uriBuilder = new UriBuilder(baseUrl);
            uriBuilder.Path = GameItemsEndpoint;

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            foreach (var keyValuePair in qParams) query[keyValuePair.Key] = keyValuePair.Value;
            uriBuilder.Query = query.ToString();

            using var unityWebRequest = UnityWebRequest.Get(uriBuilder.Uri);
            unityWebRequest.SetRequestHeader("Content-Type", "application/json");
            unityWebRequest.SetRequestHeader("Authorization", jwtToken);
            unityWebRequest.SetRequestHeader("X-SDK-VERSION", SdkVersion);
            yield return unityWebRequest.SendWebRequest();

            onResult.Invoke(WebRequestUtils.IsSuccess(unityWebRequest)
                ? Result<List<GameItem>>.Success(GameItemsResp.ToGameItems(unityWebRequest.downloadHandler.text))
                : Result<List<GameItem>>.Error(WebRequestUtils.GetError(unityWebRequest)));
        }

        internal IEnumerator Download(string url, string outPath, Action<int> progress, Action<Result<bool>> onResult)
        {
            using var webRequest = UnityWebRequest.Get(url);
            var fileHandler = new DownloadHandlerFile(outPath);
            webRequest.downloadHandler = fileHandler;
            webRequest.SendWebRequest();
            var oldProgress = 0;

            while (!webRequest.isDone)
            {
                var newProgress = (int)Math.Ceiling(webRequest.downloadProgress * 100f);
                if (oldProgress != newProgress)
                {
                    progress(newProgress);
                    oldProgress = newProgress;
                }

                yield return null;
            }

            onResult.Invoke(WebRequestUtils.IsSuccess(webRequest)
                ? Result<bool>.Success(true)
                : Result<bool>.Error(WebRequestUtils.GetError(webRequest)));
        }
    }
}