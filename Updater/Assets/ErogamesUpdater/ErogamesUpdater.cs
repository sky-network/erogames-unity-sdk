using ErogamesUpdaterNS.Entity;
using UnityEngine;

namespace ErogamesUpdaterNS
{
    public static class ErogamesUpdater
    {
        public static void Init(string accessKey, string whitelabelId, string locale)
        {
            Init(accessKey, whitelabelId, Application.identifier, locale);
        }

        public static void Init(string accessKey, string whitelabelId, string appId, string locale)
        {
            ErogamesUpdaterBridge.Instance.Init(accessKey, whitelabelId, appId, locale);
        }

        public static void Update(IUpdateListener listener)
        {
            ErogamesUpdaterBridge.Instance.UpdateIt(listener);
        }

        public static void Stop()
        {
            ErogamesUpdaterBridge.Instance.Stop();
        }
    }
}