﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class GameItem
    {
        public string slug;
        public string app_id;
        public string download_url;
        public string rawVersionCode;
        public List<Patch> patches = new();

        internal Patch GetPatch(string oldVersion)
        {
            return patches.FirstOrDefault(patch => patch.old_version == oldVersion);
        }
    }

    [Serializable]
    internal class Patch
    {
        public string old_version;
        public string url;
        public bool dir_diff = true;
    }
}