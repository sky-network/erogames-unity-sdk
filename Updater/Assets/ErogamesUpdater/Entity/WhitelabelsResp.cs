﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ErogamesUpdaterNS.Entity;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class WhitelabelsResp
    {
        public static List<Whitelabel> ToWhitelabels(string json)
        {
            var jsonObj = JSON.Parse(json);
            var whitelabelsNode = jsonObj["whitelabels"].AsArray;
            var whitelabels = new List<Whitelabel>();

            foreach (JSONNode whitelabelNode in whitelabelsNode)
            {
                var whitelabel = JsonUtility.FromJson<Whitelabel>(whitelabelNode.ToString());
                whitelabels.Add(whitelabel);
            }
            return whitelabels;
        }

        public static Whitelabel ToWhitelabel(string json)
        {
            var jsonObj = JSON.Parse(json);

            var whitelabel = JsonUtility.FromJson<Whitelabel>(jsonObj.ToString());
            return whitelabel;
        }
    }
}
