﻿namespace ErogamesUpdaterNS.Entity
{
    public interface IUpdateListener
    {
        public void OnResult(UpdateState state);
    }
}