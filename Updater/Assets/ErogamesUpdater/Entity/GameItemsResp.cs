﻿using System;
using System.Collections.Generic;
using ErogamesUpdaterNS.Entity;
using UnityEngine;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class GameItemsResp
    {
        internal static List<GameItem> ToGameItems(string json)
        {
            var jsonObj = JSON.Parse(json);
            var itemsNode = jsonObj["items"].AsArray;
            var gameItems = new List<GameItem>();

            foreach (JSONNode itemNode in itemsNode)
            {
                var gameItem = JsonUtility.FromJson<GameItem>(itemNode.ToString());
                gameItems.Add(gameItem);
            }

            return gameItems;
        }
    }
}