﻿namespace ErogamesAuthNS.Entity
{
    public abstract class Result<T>
    {
        public readonly T Data;
        public readonly string ErrorMsg;

        private Result(string errorMsg)
        {
            ErrorMsg = errorMsg;
        }

        private Result(T data, string errorMsg)
        {
            Data = data;
            ErrorMsg = errorMsg;
        }

        public static Result<T> Success(T data)
        {
            return new SuccessResult(data);
        }

        public static Result<T> Error(string error)
        {
            return new ErrorResult(error);
        }

        public static Result<T> Undefined()
        {
            return new UndefinedResult();
        }

        internal sealed class SuccessResult : Result<T>
        {
            public SuccessResult(T data) : base(data, null)
            {
            }
        }

        internal sealed class ErrorResult : Result<T>
        {
            public ErrorResult(string error) : base(error)
            {
            }
        }

        private sealed class UndefinedResult : Result<T>
        {
            public UndefinedResult() : base("Undefined error message")
            {
            }
        }
    }
}