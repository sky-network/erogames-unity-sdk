﻿using System;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class WlConfig
    {
        internal readonly string AppId;
        internal readonly string WlId;
        internal readonly string AccessKey;
        internal readonly string Locale;

        internal WlConfig(string appId, string wlId, string accessKey, string locale)
        {
            AppId = appId;
            WlId = wlId;
            AccessKey = accessKey;
            Locale = locale;
        }
    }
}