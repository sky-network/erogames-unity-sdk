﻿namespace ErogamesUpdaterNS.Entity
{
    public class UpdateState
    {
        public readonly State state;
        public readonly int progress;
        public readonly string message;

        public UpdateState(State state = State.Idle, int progress = 0, string message = null)
        {
            this.state = state;
            this.progress = progress;
            this.message = message;
        }
    }

    public enum State
    {
        Idle,
        Running,
        Done,
        Failure
    }
}