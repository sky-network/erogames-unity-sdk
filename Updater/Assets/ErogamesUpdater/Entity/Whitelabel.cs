﻿using System;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class Whitelabel
    {
        public string slug;
        public string name;
        public string url;
    }
}
