﻿using System;
using System.Collections.Generic;

namespace ErogamesAuthNS.Entity
{
    [Serializable]
    internal class ErrorResp
    {
        public string message;
        public List<string> errors;
        public string error_description;
    }
}
