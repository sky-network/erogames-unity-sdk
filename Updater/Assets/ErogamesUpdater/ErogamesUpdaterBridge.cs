using System.Collections;
using ErogamesUpdaterNS.Entity;
using UnityEngine;

namespace ErogamesUpdaterNS
{
    internal sealed class ErogamesUpdaterBridge : Singleton<ErogamesUpdaterBridge>
    {
        private bool _isUpdateRunning;
        private Coroutine _coroutine;

#if UNITY_ANDROID
        private const string BridgeClassName = "com.erogames.updater.unitywrapper.ErogamesUpdaterBridge";
        private UnityEngine.AndroidJavaClass _bridgeJavaClass;
        private IUpdateListener _updateListener;
#endif

#if UNITY_ANDROID
        private UnityEngine.AndroidJavaClass GetBridgeJavaClass()
        {
            return _bridgeJavaClass ??= new UnityEngine.AndroidJavaClass(BridgeClassName);
        }
#endif

        internal void Init(string accessKey, string wlId, string appId, string locale)
        {
#if UNITY_ANDROID
            GetBridgeJavaClass().CallStatic("init", accessKey, wlId, locale);
#else
            DesktopUpdater.Instance.Init(appId, wlId, accessKey, locale);
#endif
        }

        internal void UpdateIt(IUpdateListener listener)
        {
            if (!_isUpdateRunning)
            {
                _isUpdateRunning = true;
                _coroutine = StartCoroutine(UpdateInternal(listener));
            }
            else
            {
                Debug.Log("The updater is already in progress!");
            }
        }

        private IEnumerator UpdateInternal(IUpdateListener listener)
        {
#if UNITY_ANDROID
            _updateListener = listener;
            GetBridgeJavaClass().CallStatic("update");
            yield return null;
#else
            yield return DesktopUpdater.Instance.StartUpdate(listener);
#endif
            _isUpdateRunning = false;
        }

        internal void Stop()
        {
#if UNITY_ANDROID
            GetBridgeJavaClass().CallStatic("stop");
#else
            DesktopUpdater.Instance.Stop();
#endif
            if (_coroutine != null) StopCoroutine(_coroutine);
            _coroutine = null;
            _isUpdateRunning = false;
        }

#if UNITY_ANDROID
        internal void OnErogamesUpdaterResult(string resultStr)
        {
            var jsonObject = JSON.Parse(resultStr);
            var stateRaw = jsonObject["state"].Value;
            var progressRaw = jsonObject["progress"].Value;
            var messageRaw = jsonObject["message"].Value;

            System.Enum.TryParse(stateRaw, true, out State state);
            int.TryParse(progressRaw, out var progress);

            OnErogamesUpdaterResult(new UpdateState(state, progress, messageRaw));
        }

        private void OnErogamesUpdaterResult(UpdateState updateState)
        {
            _updateListener?.OnResult(updateState);
        }
#endif
    }
}