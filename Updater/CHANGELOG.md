# Version 1.0.3 - Nov 29, 2023

* Bug fixes.

# Version 1.0.2 - Nov 29, 2023

* Updated project dependencies.

# Version 1.0.1 - Nov 28, 2023

* Fixed dependencies conflicts.

# Version 1.0.0 - Nov 28, 2023
