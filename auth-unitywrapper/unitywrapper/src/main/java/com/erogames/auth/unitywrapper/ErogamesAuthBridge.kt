package com.erogames.auth.unitywrapper

import androidx.annotation.Keep
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.OnResult
import com.erogames.auth.model.PaymentInfo
import com.erogames.auth.model.QuestData
import com.erogames.auth.unitywrapper.ui.ErogamesAuthBridgeActivity
import com.unity3d.player.UnityPlayer
import kotlinx.serialization.encodeToString

@Keep
object ErogamesAuthBridge {

    private const val DEFAULT_LANG = "en"

    private const val UNITY_CALLBACK_OBJECT = "ErogamesAuthBridge"
    private const val UNITY_METHOD_ON_SUCCESS = "OnAuthSuccess"
    private const val UNITY_METHOD_ON_FAILURE = "OnAuthError"
    private const val UNITY_METHOD_ON_LOGOUT = "OnLogout"

    private const val UNITY_METHOD_ON_RELOAD_USER_SUCCESS = "OnReloadUserSuccess"
    private const val UNITY_METHOD_ON_RELOAD_USER_FAILURE = "OnReloadUserFailure"

    private const val UNITY_METHOD_ON_REFRESH_TOKEN_SUCCESS = "OnRefreshTokenSuccess"
    private const val UNITY_METHOD_ON_REFRESH_TOKEN_FAILURE = "OnRefreshTokenFailure"

    private const val UNITY_METHOD_ON_LOAD_WHITELABEL_SUCCESS = "OnLoadWhitelabelSuccess"
    private const val UNITY_METHOD_ON_LOAD_WHITELABEL_FAILURE = "OnLoadWhitelabelFailure"

    private const val UNITY_METHOD_ON_PROCEED_PAYMENT_SUCCESS = "OnProceedPaymentSuccess"
    private const val UNITY_METHOD_ON_PROCEED_PAYMENT_FAILURE = "OnProceedPaymentFailure"

    private const val UNITY_METHOD_ON_LOAD_QUEST_SUCCESS = "OnLoadCurrentQuestSuccess"
    private const val UNITY_METHOD_ON_LOAD_QUEST_FAILURE = "OnLoadCurrentQuestFailure"

    private const val UNITY_METHOD_ON_LOAD_PAYMENT_INFO_SUCCESS = "OnLoadPaymentInfoSuccess"
    private const val UNITY_METHOD_ON_LOAD_PAYMENT_INFO_FAILURE = "OnLoadPaymentInfoFailure"

    private const val UNITY_METHOD_ON_HAS_UPDATE_SUCCESS = "OnHasUpdateSuccess"
    private const val UNITY_METHOD_ON_HAS_UPDATE_FAILURE = "OnHasUpdateFailure"

    private const val UNITY_METHOD_ON_OPEN_IN_STORE_SUCCESS = "OnOpenInStoreSuccess"
    private const val UNITY_METHOD_ON_OPEN_IN_STORE_FAILURE = "OnOpenInStoreFailure"

    @JvmStatic
    fun init(clientId: String) {
        UnityPlayer.currentActivity?.let { ErogamesAuth.init(it, clientId) }
    }

    @JvmStatic
    fun signup(locale: String = DEFAULT_LANG) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.signup(it, locale)
        }
    }

    @JvmStatic
    fun login(locale: String = DEFAULT_LANG) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.login(it, locale)
        }
    }

    @JvmStatic
    @JvmOverloads
    fun logout(webLogout: Boolean = false) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.logout(it, webLogout)
        }
    }

    @JvmStatic
    fun getUser(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getUser(activity)
                ?.let { user -> return InjectorUtil.myJson.encodeToString(user) }
        }
        return null
    }

    @JvmStatic
    fun reloadUser() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.reloadUser(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_RELOAD_USER_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_RELOAD_USER_FAILURE,
                    t?.message ?: "reloadUser: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun getToken(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getToken(activity)
                ?.let { token -> return InjectorUtil.myJson.encodeToString(token) }
        }
        return null
    }

    @JvmStatic
    fun refreshToken() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.refreshToken(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_REFRESH_TOKEN_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_REFRESH_TOKEN_FAILURE,
                    t?.message ?: "refreshToken: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun getWhitelabel(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getWhitelabel(activity)
                ?.let { whitelabel ->
                    return InjectorUtil.myJson.encodeToString(
                        whitelabel
                    )
                }
        }
        return null
    }

    @JvmStatic
    fun loadWhitelabel() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadWhitelabel(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_WHITELABEL_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_WHITELABEL_FAILURE,
                    t?.message ?: "loadWhitelabel: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun proceedPayment(paymentId: String, amount: Int) {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.proceedPayment(activity, paymentId, amount, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_PROCEED_PAYMENT_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_PROCEED_PAYMENT_FAILURE,
                    t?.message ?: "proceedPayment: unknown error"
                )
            })
        }
    }
    
    @JvmStatic
    fun loadCurrentQuest() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadCurrentQuest(activity, object : OnResult<QuestData> {
                override fun onSuccess(data: QuestData) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_QUEST_SUCCESS,
                    InjectorUtil.myJson.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_QUEST_FAILURE,
                    t?.message ?: "loadCurrentQuest: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun loadPaymentInfo(paymentId: String) {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadPaymentInfo(activity, paymentId, object : OnResult<PaymentInfo> {
                override fun onSuccess(data: PaymentInfo) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_PAYMENT_INFO_SUCCESS,
                    InjectorUtil.myJson.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_PAYMENT_INFO_FAILURE,
                    t?.message ?: "loadPaymentInfo: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun hasUpdate() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.hasUpdate(activity, object : OnResult<Boolean> {
                override fun onSuccess(data: Boolean) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_HAS_UPDATE_SUCCESS,
                    InjectorUtil.myJson.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_HAS_UPDATE_FAILURE,
                    t?.message ?: "hasUpdate: unknown error"
                )
            })

        }
    }

    @JvmStatic
    fun openInAppStore() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.openInAppStore(activity, object : OnResult<Boolean> {
                override fun onSuccess(data: Boolean) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_OPEN_IN_STORE_SUCCESS,
                    InjectorUtil.myJson.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_OPEN_IN_STORE_FAILURE,
                    t?.message ?: "openInAppStore: unknown error"
                )
            })
        }
    }


    fun sendAuthSuccess() {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_SUCCESS,
            ""
        )
    }

    fun sendAuthFailure(error: String) {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_FAILURE,
            error
        )
    }

    fun sendLogout() {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_LOGOUT,
            ""
        )
    }
}
