package com.erogames.auth.unitywrapper

import kotlinx.serialization.json.Json

/**
 * Simple injector.
 */
internal object InjectorUtil {
    val myJson: Json by lazy { Json { ignoreUnknownKeys = true } }
}