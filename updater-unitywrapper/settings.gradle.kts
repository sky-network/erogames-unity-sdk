import java.util.Properties
pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://gitlab.com/api/v4/projects/20889762/packages/maven")
        }
    }
}

rootProject.name = "android-updater-unitywrapper"
include(":unitywrapper")
include(":sample")