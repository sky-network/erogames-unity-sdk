package com.erogames.updater.unitywrapper

import androidx.annotation.Keep
import com.erogames.updater.ErogamesUpdater
import com.erogames.updater.service.UpdateListener
import com.erogames.updater.service.UpdateState
import com.unity3d.player.UnityPlayer
import kotlinx.serialization.encodeToString
import java.util.Locale

@Keep
object ErogamesUpdaterBridge {

    private const val UNITY_CALLBACK_OBJECT = "ErogamesUpdaterBridge"
    private const val UNITY_METHOD_ON_EROGAMES_UPDATER_RESULT = "OnErogamesUpdaterResult"

    @JvmStatic
    fun init(accessKey: String, whitelabelId: String, locale: String) {
        UnityPlayer.currentActivity?.let {
            ErogamesUpdater.init(it.applicationContext, accessKey, whitelabelId, Locale(locale))
        }
    }

    @JvmStatic
    fun update() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesUpdater.update(activity.applicationContext, object : UpdateListener {
                override fun onResult(state: UpdateState?) {
                    state?.let {
                        val stateStr = InjectorUtil.myJson.encodeToString(it)
                        UnityPlayer.UnitySendMessage(
                            UNITY_CALLBACK_OBJECT,
                            UNITY_METHOD_ON_EROGAMES_UPDATER_RESULT,
                            stateStr
                        )
                    }
                }
            })
        }
    }

    @JvmStatic
    fun stop() {
        UnityPlayer.currentActivity?.let { ErogamesUpdater.stop(it.applicationContext) }
    }
}
