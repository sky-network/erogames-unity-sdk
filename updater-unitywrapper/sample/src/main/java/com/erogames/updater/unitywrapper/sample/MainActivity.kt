package com.erogames.updater.unitywrapper.sample

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.erogames.updater.unitywrapper.ErogamesUpdaterBridge

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val permissions =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            arrayOf(Manifest.permission.POST_NOTIFICATIONS)
        else arrayOf()

    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.entries.firstOrNull { entry1 -> !entry1.value }?.let { entry2 ->
                Toast.makeText(this, "${entry2.key} is denied", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val accessKey = "<accessKey>"
        val whitelabelId = "erogames"
        ErogamesUpdaterBridge.init(accessKey, whitelabelId, "en")

        findViewById<View>(R.id.updateButton).setOnClickListener {
            if (hasPermissions()) {
                ErogamesUpdaterBridge.update()
            } else {
                permissionLauncher.launch(permissions)
            }
        }
        findViewById<View>(R.id.cancelButton).setOnClickListener { ErogamesUpdaterBridge.stop() }
    }

    private fun hasPermissions(): Boolean {
        val atLeastTiramisu = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
        val hasPermission = when {
            atLeastTiramisu -> hasPermission(Manifest.permission.POST_NOTIFICATIONS)
            else -> true
        }
        if (!hasPermission) {
            Log.w(TAG, "Permission 'POST_NOTIFICATIONS' is denied.")
        }
        return hasPermission
    }

    private fun hasPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    companion object {
        const val TAG = "MainActivity"
    }
}