﻿const SessionStorage = {

    TakeCodeFromIframe__deps: ['GetQueryValue'],
    TakeCodeFromIframe: function (url) {
        url = Pointer_stringify(url);
        var codeValue = null;
        var iframe = document.createElement("iframe");
        iframe.name = "com_erogames_sdk_auth_iframe";
        iframe.style.display = "none";
        iframe.onload = function () {
            try {
                var queryParams = iframe.contentWindow.location.search;
                codeValue = _GetQueryValue(queryParams, "code", null);
            } catch (e) {
                console.log(e);
            }

            const payload = JSON.stringify({value: codeValue, origin: url});
            SendMessage('ErogamesAuthBridge', 'OnAuhCode', payload);

            iframe.parentNode.removeChild(iframe);
        }
        iframe.src = url;
        document.body.appendChild(iframe);
    },

    GetQueryValue: function (locationSearch, queryParam, defaultValue) {
        queryParam = queryParam.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + queryParam + '=([^&#]*)');
        var results = regex.exec(locationSearch);
        if(results === null) {
            return defaultValue;
        }
        return decodeURIComponent(results[1].replace(/\+/g, ' '));
    },

    SetItem: function (key, value) {
        sessionStorage.setItem(Pointer_stringify(key), Pointer_stringify(value));
    },

    GetItem: function (key) {
        var returnStr = sessionStorage.getItem(Pointer_stringify(key));
        if (!returnStr) returnStr = "";
        const bufferSize = lengthBytesUTF8(returnStr) + 1;
        const buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
    },

    RemoveItem: function (key) {
        sessionStorage.removeItem(Pointer_stringify(key));
    },

    HasKey: function (key) {
        return sessionStorage.getItem(Pointer_stringify(key)) ? 1 : 0;
    },

    GetWindowNameInternal: function () {
        var returnStr = window.name;
        const bufferSize = lengthBytesUTF8(returnStr) + 1;
        const buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
    },

    ReplaceHistoryStateInternal: function (url) {
        url = Pointer_stringify(url);
        window.history.replaceState(null, "", url);
    },

    WindowLocationReplaceInternal: function (url) {
        url = Pointer_stringify(url);
        window.location.replace(url);
    },

    WebLogoutInternal: function (url) {
      url = Pointer_stringify(url);
      var iframe = document.createElement("iframe");
      iframe.name = "com_erogames_sdk_auth_iframe_logout";
      iframe.style.display = "none";
      iframe.onload = function () {
          iframe.parentNode.removeChild(iframe);
      }
      iframe.src = url;
      document.body.appendChild(iframe);
    }
};

mergeInto(LibraryManager.library, SessionStorage);
