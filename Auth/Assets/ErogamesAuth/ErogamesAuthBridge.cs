using UnityEngine;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Repository;
using ErogamesAuthNS.Util;
using System;
using System.Collections;
using ErogamesAuthNS.Api;
using ErogamesAuthNS.UI;
using System.IO;
using System.Runtime.InteropServices;
#if UNITY_STANDALONE
using ErogamesAuthNS.Standalone;
#endif

namespace ErogamesAuthNS
{
    public delegate void OnAuthCallback(User user, string error);

    public delegate void OnResult<T>(T data);

    public delegate void OnResultSuccessDelegate();

    public delegate void OnResultFailureDelegate(string error);

    internal sealed class ErogamesAuthBridge : Singleton<ErogamesAuthBridge>
    {
        private const string BridgeClassName = "com.erogames.auth.unitywrapper.ErogamesAuthBridge";
        private AndroidJavaClass bridgeJavaClass;

        private readonly Action<Result<bool>> onAuthResult = (resp) =>
        {
            if (resp is Result<bool>.SuccessResult)
                Instance.OnAuthSuccess();
            else
                Instance.OnAuthError(resp.ErrorMsg);
        };

        private event OnAuthCallback OnAuthCallback;
        private User OnAuthSuccessPending;
        private string OnAuthErrorPending;

        private event OnResultSuccessDelegate OnReloadUserSuccessEvent;
        private event OnResultFailureDelegate OnReloadUserFailureEvent;

        private event OnResultSuccessDelegate OnRefreshTokenSuccessEvent;
        private event OnResultFailureDelegate OnRefreshTokenFailureEvent;

        private event OnResultSuccessDelegate OnProceedPaymentSuccessEvent;
        private event OnResultFailureDelegate OnProceedPaymentFailureEvent;

        private event OnResultSuccessDelegate OnLoadWhitelabelSuccessEvent;
        private event OnResultFailureDelegate OnLoadWhitelabelFailureEvent;

        private event OnResult<QuestData> OnLoadCurrentQuestSuccessEvent;
        private event OnResultFailureDelegate OnLoadCurrentQuestFailureEvent;

        private event OnResult<PaymentInfo> OnLoadPaymentInfoSuccessEvent;
        private event OnResultFailureDelegate OnLoadPaymentInfoFailureEvent;

        private event OnResult<bool> OnHasUpdateSuccessEvent;
        private event OnResultFailureDelegate OnHasUpdateFailureEvent;

        private BaseRepository Repository;
        private BaseAuthFlow _authFlow;

        private BaseAuthFlow AuthFlow
        {
            get
            {
                if (_authFlow == null)
                {
#if UNITY_WEBGL
                    _authFlow = new WebGLAuthFlow(Repository, onAuthResult);
#elif UNITY_STANDALONE
                    _authFlow = new StandaloneAuthFlow(Repository, onAuthResult);
#endif
                }

                return _authFlow;
            }
        }

        internal void Init()
        {
            var apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            var apiService = new ApiService();
#if UNITY_WEBGL
            var storage = new LSStorage();
#else
            var storage = new PlayerPrefsStorage();
#endif
            Debug.Log("Init Repository");
            Debug.Log("whitelabelId: " + apiPrefs.whitelabelId);
            Repository = new BaseRepository(
                apiPrefs.clientId,
                apiPrefs.accessKey,
                apiPrefs.whitelabelId,
                apiService,
                storage);
            Repository.WithAutoLogin(apiPrefs.autoLogin);
            var wl = Repository.GetWhitelabel();
            if (wl != null && apiPrefs.whitelabelId != wl.slug) Repository.ClearData();
        }


#if UNITY_WEBGL
        internal IEnumerator OnAuhCode(string codeObjJson)
        {
            Code code = JsonUtility.FromJson<Code>(codeObjJson);
            if (!string.IsNullOrEmpty(code.value))
            {
                yield return ProceedAuth(code.value);
            }
            else
            {
                WebGLAuthFlow.WindowLocationReplace(code.origin);
                yield break;
            }
        }
#endif

        internal IEnumerator ProceedAuth(string code)
        {
            yield return AuthFlow.ProceedAuth(code);
        }

        internal void SetAuthCallback(OnAuthCallback callback)
        {
            OnAuthCallback = callback;
            if (OnAuthSuccessPending != null)
            {
                OnAuthCallback(OnAuthSuccessPending, null);
                OnAuthSuccessPending = null;
                OnAuthErrorPending = null;
            }

            if (OnAuthErrorPending != null)
            {
                OnAuthCallback(null, OnAuthErrorPending);
                OnAuthSuccessPending = null;
                OnAuthErrorPending = null;
            }
        }

        internal void OnAuthSuccess()
        {
            ProgressBar.Hide();
            Repository.IsOnLogin(false);
            OnAuthSuccessPending = GetUser();
            OnAuthErrorPending = null;
            if (OnAuthCallback != null) OnAuthCallback(OnAuthSuccessPending, null);
        }

        internal void OnAuthError(string error)
        {
            ProgressBar.Hide();
            Repository.IsOnLogin(false);
            OnAuthSuccessPending = null;
            OnAuthErrorPending = error;
            if (OnAuthCallback != null) OnAuthCallback(null, OnAuthErrorPending);
        }

        internal void OnLogout()
        {
            if (OnAuthCallback != null) OnAuthCallback(null, null);
        }

        internal void OnReloadUserSuccess()
        {
            if (OnReloadUserSuccessEvent != null) OnReloadUserSuccessEvent.Invoke();
        }

        internal void OnReloadUserFailure(string error)
        {
            if (OnReloadUserFailureEvent != null) OnReloadUserFailureEvent.Invoke(error);
        }

        internal void OnRefreshTokenSuccess()
        {
            if (OnRefreshTokenSuccessEvent != null) OnRefreshTokenSuccessEvent.Invoke();
        }

        internal void OnRefreshTokenFailure(string error)
        {
            if (OnRefreshTokenFailureEvent != null) OnRefreshTokenFailureEvent.Invoke(error);
        }

        internal void OnProceedPaymentSuccess()
        {
            if (OnProceedPaymentSuccessEvent != null) OnProceedPaymentSuccessEvent.Invoke();
        }

        internal void OnProceedPaymentFailure(string error)
        {
            if (OnProceedPaymentFailureEvent != null) OnProceedPaymentFailureEvent.Invoke(error);
        }

        internal void OnLoadWhitelabelSuccess()
        {
            if (OnLoadWhitelabelSuccessEvent != null) OnLoadWhitelabelSuccessEvent.Invoke();
        }

        internal void OnLoadWhitelabelFailure(string error)
        {
            if (OnLoadWhitelabelFailureEvent != null) OnLoadWhitelabelFailureEvent.Invoke(error);
        }

        internal void OnLoadCurrentQuestSuccess(string questDataJson)
        {
            if (OnLoadCurrentQuestSuccessEvent != null)
            {
                QuestData questData = JsonUtility.FromJson<QuestData>(questDataJson);
                OnLoadCurrentQuestSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadCurrentQuestSuccess(QuestData questData)
        {
            if (OnLoadCurrentQuestSuccessEvent != null)
            {
                OnLoadCurrentQuestSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadCurrentQuestFailure(string error)
        {
            if (OnLoadCurrentQuestFailureEvent != null) OnLoadCurrentQuestFailureEvent.Invoke(error);
        }

        internal void OnLoadPaymentInfoSuccess(string paymentInfoJson)
        {
            if (OnLoadPaymentInfoSuccessEvent != null)
            {
                PaymentInfo questData = JsonUtility.FromJson<PaymentInfo>(paymentInfoJson);
                OnLoadPaymentInfoSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadPaymentInfoSuccess(PaymentInfo paymentInfo)
        {
            if (OnLoadPaymentInfoSuccessEvent != null)
            {
                OnLoadPaymentInfoSuccessEvent.Invoke(paymentInfo);
            }
        }

        internal void OnLoadPaymentInfoFailure(string error)
        {
            if (OnLoadPaymentInfoFailureEvent != null) OnLoadPaymentInfoFailureEvent.Invoke(error);
        }

        internal void OnHasUpdateSuccess(Boolean hasUpdate)
        {
            if (OnHasUpdateSuccessEvent != null) OnHasUpdateSuccessEvent(hasUpdate);
        }

        internal void OnHasUpdateSuccess(string hasUpdateJson)
        {
            if (OnHasUpdateSuccessEvent != null)
            {
                OnHasUpdateSuccessEvent(bool.Parse(hasUpdateJson));
            }
        }

        internal void OnHasUpdateFailure(string error)
        {
            if (OnHasUpdateFailureEvent != null) OnHasUpdateFailureEvent(error);
        }

        private AndroidJavaClass GetBridgeJavaClass()
        {
            if (bridgeJavaClass == null)
                bridgeJavaClass = new AndroidJavaClass(BridgeClassName);
            return bridgeJavaClass;
        }

        internal IEnumerator Login(string language)
        {
            Repository.IsOnLogin(true);
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.WebGLPlayer:
                case RuntimePlatform.OSXPlayer:
                    ProgressBar.Show(Strings.Get(Strings.LoggingIn, language));
                    yield return AuthFlow.StartAuth(language);
                    break;
                case RuntimePlatform.Android:
                    GetBridgeJavaClass().CallStatic("init", Repository.GetClientId());
                    GetBridgeJavaClass().CallStatic("login", language);
                    break;
                default:
                    throw new NotSupportedException("Not supported for " + Application.platform);
            }
        }

        internal IEnumerator Signup(string language)
        {
            Repository.IsOnLogin(true);
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.WebGLPlayer:
                case RuntimePlatform.OSXPlayer:
                    ProgressBar.Show(Strings.Get(Strings.SigningUp, language));
                    yield return AuthFlow.StartAuth(language, true);
                    break;
                case RuntimePlatform.Android:
                    GetBridgeJavaClass().CallStatic("init", Repository.GetClientId());
                    GetBridgeJavaClass().CallStatic("signup", language);
                    break;
                default:
                    throw new NotSupportedException("Not supported for " + Application.platform);
            }
        }

        internal void Logout(bool webLogout)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.WebGLPlayer:
                case RuntimePlatform.OSXPlayer:
                    AuthFlow.Logout(webLogout);
                    OnLogout();
                    break;
                case RuntimePlatform.Android:
                    GetBridgeJavaClass().CallStatic("logout", webLogout);
                    break;
                default:
                    throw new NotSupportedException("Not supported for " + Application.platform);
            }
        }

        internal User GetUser()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string userStr = GetBridgeJavaClass().CallStatic<string>("getUser");
                return JsonUtility.FromJson<User>(userStr);
            }

            return Repository.GetUser();
        }

        internal IEnumerator ReloadUser(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnReloadUserSuccessEvent = onSuccess;
            OnReloadUserFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("reloadUser");
                yield break;
            }

            Result<User> reloadUserResult = Result<User>.Undefined();
            yield return Repository.LoadUser((result) => reloadUserResult = result);
            if (reloadUserResult is Result<User>.ErrorResult)
            {
                OnReloadUserFailure(reloadUserResult.ErrorMsg);
                yield break;
            }

            OnReloadUserSuccess();
        }

        internal Token GetToken()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string tokenStr = GetBridgeJavaClass().CallStatic<string>("getToken");
                return JsonUtility.FromJson<Token>(tokenStr);
            }

            return Repository.GetToken();
        }

        internal IEnumerator RefreshToken(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnRefreshTokenSuccessEvent = onSuccess;
            OnRefreshTokenFailureEvent = onFailure;
            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("refreshToken");
                yield break;
            }

            Result<Token> refreshTokenResult = Result<Token>.Undefined();
            yield return Repository.RefreshToken((result) => refreshTokenResult = result);
            if (refreshTokenResult is Result<Token>.ErrorResult)
            {
                OnRefreshTokenFailure(refreshTokenResult.ErrorMsg);
                yield break;
            }

            OnRefreshTokenSuccess();
        }

        internal IEnumerator ProceedPayment(string paymentId, int amount, OnResultSuccessDelegate onSuccess,
            OnResultFailureDelegate onFailure)
        {
            OnProceedPaymentSuccessEvent = onSuccess;
            OnProceedPaymentFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("proceedPayment", paymentId, amount);
                yield break;
            }

            Result<BaseStatusResp> proceedPaymentResult = Result<BaseStatusResp>.Undefined();
            yield return Repository.ProceedPayment(paymentId, amount, (r) => { proceedPaymentResult = r; });
            if (proceedPaymentResult is Result<BaseStatusResp>.ErrorResult)
            {
                OnProceedPaymentFailure(proceedPaymentResult.ErrorMsg);
                yield break;
            }

            OnProceedPaymentSuccess();
        }


        internal Whitelabel GetWhitelabel()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string whitelabelStr = GetBridgeJavaClass().CallStatic<string>("getWhitelabel");
                return WhitelabelsResp.ToWhitelabel(whitelabelStr);
            }

            return Repository.GetWhitelabel();
        }

        internal IEnumerator LoadWhitelabel(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnLoadWhitelabelSuccessEvent = onSuccess;
            OnLoadWhitelabelFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("loadWhitelabel");
                yield break;
            }

            Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
            yield return Repository.LoadWhitelabel((r) => { whitelabelResult = r; });
            if (whitelabelResult is Result<Whitelabel>.ErrorResult)
            {
                OnLoadWhitelabelFailure(whitelabelResult.ErrorMsg);
                yield break;
            }

            OnLoadWhitelabelSuccess();
        }

        internal IEnumerator LoadCurrentQuest(OnResult<QuestData> onSuccess, OnResultFailureDelegate onFailure)
        {
            OnLoadCurrentQuestSuccessEvent = onSuccess;
            OnLoadCurrentQuestFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("loadCurrentQuest");
                yield break;
            }

            Result<QuestData> result = Result<QuestData>.Undefined();
            yield return Repository.GetCurrentQuestData((r) => { result = r; });
            if (!(result is Result<QuestData>.SuccessResult))
            {
                OnLoadCurrentQuestFailure(result.ErrorMsg);
                yield break;
            }

            OnLoadCurrentQuestSuccess(result.Data);
        }

        internal IEnumerator LoadPaymentInfo(string paymentId, OnResult<PaymentInfo> onSuccess,
            OnResultFailureDelegate onFailure)
        {
            OnLoadPaymentInfoSuccessEvent = onSuccess;
            OnLoadPaymentInfoFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("loadPaymentInfo", paymentId);
                yield break;
            }

            Result<PaymentInfo> result = Result<PaymentInfo>.Undefined();

            yield return Repository.GetPaymentInfo(paymentId, (r) => { result = r; });
            if (!(result is Result<PaymentInfo>.SuccessResult))
            {
                OnLoadPaymentInfoFailure(result.ErrorMsg);
                yield break;
            }

            OnLoadPaymentInfoSuccess(result.Data);
        }

        internal IEnumerator HasUpdate(string locale, OnResult<bool> onSuccess, OnResultFailureDelegate onFailure)
        {
            OnHasUpdateSuccessEvent = onSuccess;
            OnHasUpdateFailureEvent = onFailure;

            if (Application.platform == RuntimePlatform.Android)
            {
                GetBridgeJavaClass().CallStatic("hasUpdate");
                yield break;
            }
            else if (Application.platform == RuntimePlatform.OSXPlayer
                     || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                Result<bool> result = Result<bool>.Undefined();
                string appId = Application.identifier;
                yield return new WaitUntil(() =>
                {
                    string path = Application.dataPath;
                    path = Path.Combine(path, "erogames_app_identifier");
                    var identifier = Utils.ReadChars(path, 100);
                    if (!string.IsNullOrEmpty(identifier)) appId = identifier;
                    return true;
                });

                string currentVersion = Application.version;

                yield return Repository.HasUpdate(appId, locale, currentVersion, (r) => { result = r; });
                if (result is Result<bool>.ErrorResult)
                {
                    OnHasUpdateFailure(result.ErrorMsg);
                    yield break;
                }

                OnHasUpdateSuccess(result.Data);
            }
            else if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                OnHasUpdateSuccess(false);
            }
            else
            {
                OnHasUpdateFailure("Not supported for " + Application.platform);
            }
        }

        internal IEnumerator OpenInAppStore(string locale, OnResult<string> onFailure)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                onFailure("Not supported for " + Application.platform);
                yield break;
            }

            var gameId = Application.identifier;
            yield return new WaitUntil(() =>
            {
                string path = Application.dataPath;
                path = Path.Combine(path, "erogames_app_identifier");
                var identifier = Utils.ReadChars(path, 100);
                if (!string.IsNullOrEmpty(identifier)) gameId = identifier;
                return true;
            });
            Debug.Log("gameId: " + gameId);

            Result<Whitelabel> wlResult = Result<Whitelabel>.Undefined();
            yield return Repository.GetOrLoadWhitelabel((result) => { wlResult = result; });
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onFailure(wlResult.ErrorMsg);
                yield break;
            }

            var appStoreId = (Application.platform == RuntimePlatform.OSXPlayer)
                ? wlResult.Data.macos_app_store_id
                : wlResult.Data.win_app_store_id;
            if (string.IsNullOrEmpty(appStoreId)) appStoreId = "com-erogames-app";
            var appStoreLink = string.Format("{0}://details?id={1}", appStoreId, gameId);
            if (CanOpenAppStoreLink(appStoreLink))
            {
                OpenAppStoreLink(appStoreLink);
                yield break;
            }

            Result<GameItem> gameResult = Result<GameItem>.Undefined();
            yield return Repository.GetOrLoadGameItem(gameId, locale, (result) => gameResult = result);
            if (gameResult is Result<GameItem>.ErrorResult)
            {
                onFailure(gameResult.ErrorMsg);
                yield break;
            }

            string gameWebPath = gameResult.Data.game_page.TrimStart('/');
            string webGameUrl = wlResult.Data.url.TrimEnd('/') + "/" + gameWebPath;
            Debug.Log("webGameUrl: " + webGameUrl);
            Application.OpenURL(webGameUrl);
        }

        private bool CanOpenAppStoreLink(string appStoreLink)
        {
#if UNITY_STANDALONE_WIN
            return ErogamesAuthPlugin.CanOpenURL(new Uri(appStoreLink).Scheme);
#elif UNITY_STANDALONE_OSX || UNITY_ANDROID
            return ErogamesAuthPlugin.CanOpenURL(appStoreLink);
#else
            return false;
#endif
        }

        private void OpenAppStoreLink(string appStoreLink)
        {
#if UNITY_ANDROID
            ErogamesAuthPlugin.StartActivity(appStoreLink);
#else
            Application.OpenURL(appStoreLink);
#endif
        }

        internal bool IsOnLogin()
        {
            return Repository.IsOnLogin();
        }

        internal void IsOnLogin(bool onLogin)
        {
            Repository.IsOnLogin(onLogin);
        }

        internal bool WithAutoLogin()
        {
            return Repository.WithAutoLogin();
        }

        protected override void OnDestroy()
        {
            Repository.IsOnLogin(false);
        }
    }
}