﻿using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Repository
{
    internal class PlayerPrefsStorage : IStorage
    {
        private const string StorageKeyUser = "com_erogames_sdk_unity_storage_user";
        private const string StorageKeyToken = "com_erogames_sdk_unity_storage_token";
        private const string StorageKeyWhitelabel = "com_erogames_sdk_unity_storage_whitelabel";
        private const string StorageKeyAccessKey = "com_erogames_sdk_unity_storage_access_key";
        private const string StorageKeyWhitelabelId = "com_erogames_sdk_unity_storage_whitelabel_id";
        private const string StorageKeyGameItem = "com_erogames_sdk_unity_storage_game_item";
        private const string StorageKeyClientId = "com_erogames_sdk_unity_storage_client_id";
        private const string StorageKeyOnLogin = "com_erogames_sdk_unity_storage_on_login";
        

        /// <summary>
        /// Save Token to local storage.
        /// </summary>
        /// <param name="token"></param>
        public void SaveToken(Token token)
        {
            PlayerPrefs.SetString(StorageKeyToken, JsonUtility.ToJson(token));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Get Token from local storage.
        /// </summary>
        /// <returns></returns>
        public Token GetToken()
        {
            string tokenStr = PlayerPrefs.GetString(StorageKeyToken);
            return (tokenStr != null) ? JsonUtility.FromJson<Token>(tokenStr) : null;
        }

        /// <summary>
        /// Save User to local storage.
        /// </summary>
        /// <param name="user"></param>
        public void SaveUser(User user)
        {
            PlayerPrefs.SetString(StorageKeyUser, JsonUtility.ToJson(user));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Get User from local storage.
        /// </summary>
        /// <returns></returns>
        public User GetUser()
        {
            string userStr = PlayerPrefs.GetString(StorageKeyUser);
            return (userStr != null) ? JsonUtility.FromJson<User>(userStr) : null;
        }

        /// <summary>
        /// Remove User from local storage.
        /// </summary>
        public void RemoveUser()
        {
            PlayerPrefs.DeleteKey(StorageKeyUser);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Remove Token from local storage.
        /// </summary>
        public void RemoveToken()
        {
            PlayerPrefs.DeleteKey(StorageKeyToken);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Save Whitelabel to local storage.
        /// </summary>
        /// <param name="whitelabel"></param>
        public void SetWhitelabel(Whitelabel whitelabel)
        {
            PlayerPrefs.SetString(StorageKeyWhitelabel, JsonUtility.ToJson(whitelabel));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Ret Whitelabel from local storage.
        /// </summary>
        /// <returns></returns>
        public Whitelabel GetWhitelabel()
        {
            string whitelabelStr = PlayerPrefs.GetString(StorageKeyWhitelabel);
            return (whitelabelStr != null) ? JsonUtility.FromJson<Whitelabel>(whitelabelStr) : null;
        }

        /// <summary>
        /// Remove Whitelabel from local storage.
        /// </summary>
        public void RemoveWhitelabel()
        {
            PlayerPrefs.DeleteKey(StorageKeyWhitelabel);
            PlayerPrefs.Save();
        }

        public void SetAccessKey(string accessKey)
        {
            PlayerPrefs.SetString(StorageKeyAccessKey, accessKey);
            PlayerPrefs.Save();
        }

        public string GetAccessKey()
        {
            return PlayerPrefs.GetString(StorageKeyAccessKey);
        }

        public void SetWhitelabelId(string whitelabelId)
        {
            PlayerPrefs.SetString(StorageKeyWhitelabelId, whitelabelId);
            PlayerPrefs.Save();
        }

        public string GetWhitelabelId()
        {
            return PlayerPrefs.GetString(StorageKeyWhitelabelId);
        }

        public void SetGameItem(GameItem gameItem)
        {
            PlayerPrefs.SetString(StorageKeyGameItem, JsonUtility.ToJson(gameItem));
            PlayerPrefs.Save();
        }

        public GameItem GetGameItem()
        {
            string gameItemStr = PlayerPrefs.GetString(StorageKeyGameItem);
            return (gameItemStr != null) ? JsonUtility.FromJson<GameItem>(gameItemStr) : null;
        }

        public void RemoveGameItem()
        {
            PlayerPrefs.DeleteKey(StorageKeyGameItem);
            PlayerPrefs.Save();
        }

        public void SetClientId(string clientId)
        {
            PlayerPrefs.SetString(StorageKeyClientId, clientId);
            PlayerPrefs.Save();
        }

        public string GetClientId()
        {
            return PlayerPrefs.GetString(StorageKeyClientId);
        }

        public bool IsOnLogin()
        {
            return PlayerPrefs.GetInt(StorageKeyOnLogin, 0) > 0;
        }

        public void IsOnLogin(bool onLogin)
        {
            PlayerPrefs.SetInt(StorageKeyOnLogin, onLogin ? 1 : 0);
            PlayerPrefs.Save();
        }
    }
}
