﻿using System.Runtime.InteropServices;
using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Repository
{
#if UNITY_WEBGL
    internal class LSStorage : IStorage
    {
        //private const string StorageKeyUser = "com_erogames_sdk_unity_storage_user";
        //private const string StorageKeyToken = "com_erogames_sdk_unity_storage_token";
        private const string StorageKeyWhitelabel = "com_erogames_sdk_unity_storage_whitelabel";
        private const string StorageKeyAccessKey = "com_erogames_sdk_unity_storage_access_key";
        private const string StorageKeyWhitelabelId = "com_erogames_sdk_unity_storage_whitelabel_id";
        private const string StorageKeyGameItem = "com_erogames_sdk_unity_storage_game_item";
        private const string StorageKeyClientId = "com_erogames_sdk_unity_storage_client_id";
        private const string StorageKeyOnLogin = "com_erogames_sdk_unity_storage_on_login";

        [DllImport("__Internal")]
        internal static extern void SetItem(string key, string value);

        [DllImport("__Internal")]
        internal static extern string GetItem(string key);

        [DllImport("__Internal")]
        internal static extern void RemoveItem(string key);

        [DllImport("__Internal")]
        internal static extern int HasKey(string key);

        private Token token;
        private User user;

        public void SaveToken(Token token)
        {
            this.token = token;
            //SetItem(StorageKeyToken, JsonUtility.ToJson(token));
        }

        public Token GetToken()
        {
            return token;
            //string tokenStr = GetItem(StorageKeyToken);
            //return (tokenStr != null) ? JsonUtility.FromJson<Token>(tokenStr) : null;
        }

        public void RemoveToken()
        {
            token = null;
            //RemoveItem(StorageKeyToken);
        }

        public void SaveUser(User user)
        {
            this.user = user;
            //SetItem(StorageKeyUser, JsonUtility.ToJson(user));
        }

        public User GetUser()
        {
            return user;
            //string userStr = GetItem(StorageKeyUser);
            //return (userStr != null) ? JsonUtility.FromJson<User>(userStr) : null;
        }

        public void RemoveUser()
        {
            user = null;
            //RemoveItem(StorageKeyUser);
        }

        public void SetWhitelabel(Whitelabel whitelabel)
        {
            SetItem(StorageKeyWhitelabel, JsonUtility.ToJson(whitelabel));
        }

        public Whitelabel GetWhitelabel()
        {
            string whitelabelStr = GetItem(StorageKeyWhitelabel);
            return (whitelabelStr != null) ? JsonUtility.FromJson<Whitelabel>(whitelabelStr) : null;
        }

        public void RemoveWhitelabel()
        {
            RemoveItem(StorageKeyWhitelabel);
        }

        public void SetAccessKey(string accessKey)
        {
            SetItem(StorageKeyAccessKey, accessKey);
        }

        public string GetAccessKey()
        {
            return GetItem(StorageKeyAccessKey);
        }

        public void SetWhitelabelId(string whitelabelId)
        {
            SetItem(StorageKeyWhitelabelId, whitelabelId);
        }

        public string GetWhitelabelId()
        {
            return GetItem(StorageKeyWhitelabelId);
        }

        public void SetGameItem(GameItem gameItem)
        {
            SetItem(StorageKeyGameItem, JsonUtility.ToJson(gameItem));
        }

        public GameItem GetGameItem()
        {
            string gameItemStr = GetItem(StorageKeyGameItem);
            return (gameItemStr != null) ? JsonUtility.FromJson<GameItem>(gameItemStr) : null;
        }

        public void RemoveGameItem()
        {
            RemoveItem(StorageKeyGameItem);
        }

        public void SetClientId(string clientId)
        {
            SetItem(StorageKeyClientId, clientId);
        }

        public string GetClientId()
        {
            return GetItem(StorageKeyClientId);
        }

        public bool IsOnLogin()
        {
            int isOnLoginInt = 0;
            string isOnLoginStr = GetItem(StorageKeyOnLogin);
            if (int.TryParse(isOnLoginStr, out isOnLoginInt)) return isOnLoginInt > 0;
            return false;
        }

        public void IsOnLogin(bool onLogin)
        {
            SetItem(StorageKeyOnLogin, onLogin ? "1" : "0");
        }
    }
#endif
}
