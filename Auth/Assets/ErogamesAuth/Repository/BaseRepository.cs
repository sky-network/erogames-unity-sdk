﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAuthNS.Api;
using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Repository
{
    internal class BaseRepository
    {
        private readonly ApiService apiService;
        private readonly IStorage storage;
        private bool withAutoLogin = false;

        internal BaseRepository(string clientId, string accessKey, string whitelabelId, ApiService apiService, IStorage storage)
        {
            this.apiService = apiService;
            this.storage = storage;
            this.storage.SetClientId(clientId);
            this.storage.SetAccessKey(accessKey);
            this.storage.SetWhitelabelId(whitelabelId);
        }

        private string GetAccessKey()
        {
            return storage.GetAccessKey();
        }

        private string GetWhitelabelId()
        {
            return storage.GetWhitelabelId();
        }

        internal IEnumerator GetOrLoadWhitelabel(Action<Result<Whitelabel>> result)
        {
            Whitelabel wl = GetWhitelabel();
            if (wl != null && wl.slug != GetWhitelabelId())
            {
                RemoveWhitelabel();
                wl = null;
            }

            if (wl != null) result(Result<Whitelabel>.Success(wl));
            else yield return LoadWhitelabel(result);
        }

        /// <summary>
        /// Load Whitelabel and save it to local storage.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        internal IEnumerator LoadWhitelabel(Action<Result<Whitelabel>> onResult)
        {
            string whitelabelId = GetWhitelabelId();
            Result<string> jwtTokenResult = Result<string>.Undefined();
            Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();

            yield return LoadJwtToken(GetAccessKey(), (result) => jwtTokenResult = result);
            if (jwtTokenResult is Result<string>.ErrorResult)
            {
                onResult(Result<Whitelabel>.Error(jwtTokenResult.ErrorMsg));
                yield break;
            }


            yield return apiService.GetWhitelabels(jwtTokenResult.Data, (Action<Result<List<Whitelabel>>>)((wlResult) =>
            {
                if (wlResult is Result<List<Whitelabel>>.SuccessResult)
                {
                    foreach (var whitelabel in wlResult.Data)
                    {
                        if (whitelabel.slug == whitelabelId)
                        {
                            SetWhitelabel(whitelabel);
                            onResult(Result<Whitelabel>.Success(whitelabel));
                            return;
                        }
                    }
                    onResult(Result<Whitelabel>.Error(string.Format("The whitelabel '{0}' not found.", whitelabelId)));
                    return;
                }
                onResult(Result<Whitelabel>.Error(wlResult.ErrorMsg));
            }));
        }

        /// <summary>
        /// Save Whitelabel info to local storage.
        /// </summary>
        /// <param name="whitelabel"></param>
        internal void SetWhitelabel(Whitelabel whitelabel)
        {
            storage.SetWhitelabel(whitelabel);
        }

        /// <summary>
        /// Get Whitelabel from local storage.
        /// </summary>
        /// <returns>Whitelabel</returns>
        internal Whitelabel GetWhitelabel()
        {
            return storage.GetWhitelabel();
        }

        private void RemoveWhitelabel()
        {
            storage.RemoveWhitelabel();
        }


        /// <summary>
        /// Load JWT token.
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            yield return apiService.GetJwtToken(accessKey, onResult);
        }

        /// <summary>
        /// Load Token and save it to local storage.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="redirectUri"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadToken(string code, string redirectUri, string codeVerifier, Action<Result<Token>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => { wlResult = result; });
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<Token>.Error(wlResult.ErrorMsg));
                yield break;
            }

            yield return apiService.GetToken(wlResult.Data.token_url, GetClientId(), code, redirectUri, codeVerifier, (result) =>
            {
                if (result is Result<Token>.SuccessResult)
                {
                    SaveToken(result.Data);
                    onResult.Invoke(Result<Token>.Success(result.Data));
                    return;
                }
                onResult.Invoke(Result<Token>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Get client id.
        /// </summary>
        /// <returns>string</returns>
        internal string GetClientId()
        {
            return storage.GetClientId();
        }

        /// <summary>
        /// Get Token from local storage.
        /// </summary>
        /// <returns>Token</returns>
        internal Token GetToken()
        {
            return storage.GetToken();
        }

        /// <summary>
        /// Save Token to local storage.
        /// </summary>
        /// <param name="token"></param>
        internal void SaveToken(Token token)
        {
            storage.SaveToken(token);
        }

        internal IEnumerator GetOrRefreshToken(Action<Result<Token>> result)
        {
            Token token = GetToken();
            if (token != null && token.IsExpired()) yield return RefreshToken(result);
            else result(Result<Token>.Success(token));
        }

        /// <summary>
        /// Load User and save it to local storage.
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadUser(Action<Result<User>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => { wlResult = result; });
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<User>.Error(wlResult.ErrorMsg));
                yield break;
            }

            Result<Token> tokenResult = null;
            yield return GetOrRefreshToken((result) => tokenResult = result);
            if (tokenResult.Data == null)
            {
                onResult(Result<User>.Error(tokenResult.ErrorMsg));
                yield break;
            }

            yield return apiService.GetUser(wlResult.Data.profile_url, string.Format("Bearer {0}", tokenResult.Data.access_token), (result) =>
            {
                if (result is Result<User>.SuccessResult)
                {
                    SaveUser(result.Data);
                    onResult(Result<User>.Success(result.Data));
                    return;
                }
                onResult(Result<User>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Save User to local storage.
        /// </summary>
        /// <param name="user"></param>
        internal void SaveUser(User user)
        {
            storage.SaveUser(user);
        }

        /// <summary>
        /// Get User from local storage.
        /// </summary>
        /// <returns></returns>
        internal User GetUser()
        {
            return storage.GetUser();
        }

        /// <summary>
        /// Clear all local authentication data.
        /// </summary>
        internal void ClearData()
        {
            storage.RemoveUser();
            storage.RemoveToken();
            storage.RemoveWhitelabel();
            storage.RemoveGameItem();
        }


        /// <summary>
        /// Refresh Token.
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator RefreshToken(Action<Result<Token>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => { wlResult = result; });
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<Token>.Error(wlResult.ErrorMsg));
                yield break;
            }

            Token token = GetToken();
            if (token == null)
            {
                onResult(Result<Token>.Error("The token can't be null."));
                yield break;
            }

            yield return apiService.RefreshToken(wlResult.Data.token_url, GetClientId(), token.refresh_token, (result) =>
            {
                if (result is Result<Token>.SuccessResult)
                {
                    SaveToken(result.Data);
                    onResult(Result<Token>.Success(result.Data));
                    return;
                }
                onResult(Result<Token>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Proceed payment.
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="amount"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator ProceedPayment(string paymentId, int amount, Action<Result<BaseStatusResp>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => wlResult = result);
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<BaseStatusResp>.Error(wlResult.ErrorMsg));
                yield break;
            }

            Result<Token> tokenResult = null;
            yield return GetOrRefreshToken((result) => tokenResult = result);
            if (tokenResult.Data == null)
            {
                onResult.Invoke(Result<BaseStatusResp>.Error(tokenResult.ErrorMsg));
                yield break;
            }

            yield return apiService.ProceedPayment(
                wlResult.Data.url + "/api/v1/payments",
                string.Format("Bearer {0}", tokenResult.Data.access_token),
                paymentId,
                amount,
                onResult
            );
        }


        /// <summary>
        /// Get Current Quest Data
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetCurrentQuestData(Action<Result<QuestData>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => wlResult = result);
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<QuestData>.Error(wlResult.ErrorMsg));
                yield break;
            }

            Result<Token> tokenResult = null;
            yield return GetOrRefreshToken((result) => tokenResult = result);
            if (tokenResult.Data == null)
            {
                onResult(Result<QuestData>.Error(tokenResult.ErrorMsg));
                yield break;
            }

            yield return apiService.GetCurrentQuestData(
                wlResult.Data.url + "/api/v1/quests/current",
                string.Format("Bearer {0}", tokenResult.Data.access_token),
                onResult
            );
        }

        internal IEnumerator GetPaymentInfo(string paymentId, Action<Result<PaymentInfo>> onResult)
        {
            Result<Whitelabel> wlResult = null;
            yield return GetOrLoadWhitelabel((result) => wlResult = result);
            if (wlResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<PaymentInfo>.Error(wlResult.ErrorMsg));
                yield break;
            }

            Result<Token> tokenResult = null;
            yield return GetOrRefreshToken((result) => tokenResult = result);
            if (tokenResult.Data == null)
            {
                onResult(Result<PaymentInfo>.Error(tokenResult.ErrorMsg));
                yield break;
            }

            yield return apiService.GetPaymentInfo(
                wlResult.Data.url + "/api/v1/payments/" + paymentId,
                string.Format("Bearer {0}", tokenResult.Data.access_token),
                onResult
            );
        }

        internal IEnumerator HasUpdate(string appId, string locale, string currentVersionName, Action<Result<bool>> onResult)
        {
            Result<GameItem> itemResult = Result<GameItem>.Undefined();
            yield return GetOrLoadGameItem(appId, locale, (r) => { itemResult = r; });
            if (itemResult is Result<GameItem>.ErrorResult)
            {
                onResult(Result<bool>.Error(itemResult.ErrorMsg));
                yield break;
            }

            if (itemResult.Data == null || string.IsNullOrEmpty(itemResult.Data.rawVersionCode))
            {
                onResult(Result<bool>.Success(false));
                yield break;
            }

            try
            {
                Version currentVersion = new Version(currentVersionName);
                Version serverVersion = new Version(itemResult.Data.rawVersionCode);
                Debug.Log("currentVersion: " + currentVersion.ToString());
                Debug.Log("serverVersion: " + serverVersion.ToString());
                onResult(Result<bool>.Success(serverVersion.CompareTo(currentVersion) == 1));
            }
            catch (Exception e)
            {
                onResult(Result<bool>.Error(e.Message));
            }
        }

        internal IEnumerator GetOrLoadGameItem(string appId, string locale, Action<Result<GameItem>> result)
        {
            GameItem gameItem = storage.GetGameItem();
            if (gameItem != null && gameItem.app_id != appId)
            {
                RemoveGameItem();
                gameItem = null;
            }
            if (gameItem == null) yield return LoadGameItem(appId, locale, result);
            else result(Result<GameItem>.Success(gameItem));
        }

        private IEnumerator LoadGameItem(string appId, string locale, Action<Result<GameItem>> onResult)
        {
            Result<List<GameItem>> gameItemsResult = null;
            yield return LoadGameItems(locale, (result) => gameItemsResult = result);
            if (gameItemsResult is Result<List<GameItem>>.ErrorResult)
            {
                onResult(Result<GameItem>.Error(gameItemsResult.ErrorMsg));
                yield break;
            }

            GameItem gameItem = null;
            foreach (var item in gameItemsResult.Data)
            {
                if (!string.IsNullOrEmpty(item.app_id) && item.app_id == appId) gameItem = item;
            }
            if (gameItem != null)
            {
                SetGameItem(gameItem);
                onResult(Result<GameItem>.Success(gameItem));
            }
            else
            {
                onResult(Result<GameItem>.Error(string.Format("Game item ({0}) does not exist.", appId)));
            }
        }

        private void SetGameItem(GameItem gameItem)
        {
            storage.SetGameItem(gameItem);
        }

        private void RemoveGameItem()
        {
            storage.RemoveGameItem();
        }

        private IEnumerator LoadGameItems(string locale, Action<Result<List<GameItem>>> onResult)
        {
            Result<string> jwtTokenResult = null;
            Result<Whitelabel> whitelabelResult = null;

            yield return LoadJwtToken(GetAccessKey(), (result) => jwtTokenResult = result);
            if (jwtTokenResult is Result<string>.ErrorResult)
            {
                onResult(Result<List<GameItem>>.Error(jwtTokenResult.ErrorMsg));
                yield break;
            }

            yield return LoadWhitelabel((result) => whitelabelResult = result);
            if (whitelabelResult is Result<Whitelabel>.ErrorResult)
            {
                onResult(Result<List<GameItem>>.Error(jwtTokenResult.ErrorMsg));
                yield break;
            }

            var platform = Util.Utils.GetPlatformName();
            var itemsUri = string.Format("{0}{1}?locale={2}&platform={3}", whitelabelResult.Data.url, ApiService.GameItemsEndpoint, locale, platform);
            yield return apiService.GetGameItems(itemsUri, jwtTokenResult.Data, (data) => { onResult(data); });
        }

        internal bool IsOnLogin()
        {
            return storage.IsOnLogin();
        }

        internal void IsOnLogin(bool onLogin)
        {
            storage.IsOnLogin(onLogin);
        }

        internal void WithAutoLogin(bool withAutoLogin)
        {
            this.withAutoLogin = withAutoLogin;
        }

        internal bool WithAutoLogin()
        {
            return withAutoLogin;
        }
    }
}
