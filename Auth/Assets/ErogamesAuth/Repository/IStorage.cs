﻿using ErogamesAuthNS.Model;

namespace ErogamesAuthNS.Repository
{
    internal interface IStorage
    {
        
        void SetClientId(string clientId);

        string GetClientId();

        void SetAccessKey(string accessKey);

        string GetAccessKey();

        void SetWhitelabelId(string whitelabelId);

        string GetWhitelabelId();

        void SaveToken(Token token);

        Token GetToken();

        void RemoveToken();

        void SaveUser(User user);

        User GetUser();

        void RemoveUser();

        void SetWhitelabel(Whitelabel whitelabel);

        Whitelabel GetWhitelabel();

        void RemoveWhitelabel();

        void SetGameItem(GameItem gameItem);

        GameItem GetGameItem();

        void RemoveGameItem();

        bool IsOnLogin();

        void IsOnLogin(bool isOnLogin);
    }
}
