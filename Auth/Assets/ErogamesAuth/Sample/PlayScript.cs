using System;
using ErogamesAuthNS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace ErogamesAuthNS.Sample
{
    public class PlayScript : MonoBehaviour
    {
        public GameObject mainCamera;
        public Text userText;
        public Button signupButton;
        public Button loginButton;
        public Button logoutButton;
        public Button testButton;
        public Button reloadUserButton;
        public Button refreshTokenButton;

        private User user;
        private int counter;

        private void Awake()
        {
            ErogamesAuth.OnAuth((User user, string error) =>
            {
                Debug.Log("OnAuth");
                if (error != null) Debug.LogError("OnAuth error: " + error);
                UpdateUI();
            });
        }

        public void OnLogin()
        {
            ErogamesAuth.Login("en");
        }

        public void OnSignup()
        {
            ErogamesAuth.Signup("en");
        }


        public void OnLogout()
        {
            ErogamesAuth.Logout(true);
        }

        public void OnTest()
        {
            counter++;
            switch (counter)
            {
                case 1:
                    LoadWhitelabel();
                    break;
                case 2:
                    ProceedPayment();
                    break;
                case 3:
                    LoadQuestData();
                    break;
                case 4:
                    HasUpdate();
                    break;
                case 5:
                    OpenInAppStore();
                    break;
            }
            if (counter >= 5) counter = 0;
        }

        public void OnReloadUser()
        {
            ErogamesAuth.ReloadUser(
                () => UpdateUI(),
                (string error) => Debug.Log("ReloadUser error: " + error));
        }


        public void OnRefreshToken()
        {
            ErogamesAuth.RefreshToken(
                () => UpdateUI(),
                (string error) => Debug.Log("RefreshToken error: " + error));
        }


        private void UpdateUI()
        {
            user = ErogamesAuth.GetUser();
            Token token = ErogamesAuth.GetToken();
            string userStr = JsonUtility.ToJson(user);
            string tokenStr = JsonUtility.ToJson(token);

            Debug.Log("updateUI...");
            Debug.Log("user: " + userStr);
            Debug.Log("token: " + tokenStr);

            userText.text = userStr;
            signupButton.gameObject.SetActive(user == null);
            loginButton.gameObject.SetActive(user == null);
            logoutButton.gameObject.SetActive(user != null);
        }


        private void ProceedPayment()
        {
            string paymentId = Guid.NewGuid().ToString();
            int amount = 1;
            ErogamesAuth.ProceedPayment(paymentId, amount,
                () => LoadPaymentInfo(paymentId),
                (string error) => Debug.Log("OnProceedPayment error: " + error));
        }


        private void LoadPaymentInfo(string paymentId)
        {
            ErogamesAuth.LoadPaymentInfo(paymentId,
                (PaymentInfo data) => Debug.Log("LoadPaymentInfo: " + JsonUtility.ToJson(data)),
                (string error) => Debug.Log("LoadPaymentInfo error: " + error));
        }


        private void LoadWhitelabel()
        {
            ErogamesAuth.LoadWhitelabel(
                () => Debug.Log("OnLoadWhitelabel: " + ErogamesAuth.GetWhitelabel().slug),
                (string error) => Debug.Log("OnLoadWhitelabel error: " + error));
        }

        private void LoadQuestData()
        {
            ErogamesAuth.LoadCurrentQuest(
                (QuestData data) => Debug.Log("On LoadQuestData: " + JsonUtility.ToJson(data)),
                (string error) => Debug.Log("On LoadQuestData error: " + error));
        }

        private void HasUpdate()
        {
            ErogamesAuth.HasUpdate(
                "zh",
                (bool data) => Debug.Log("On HasUpdate: " + data),
                (string error) => Debug.Log("On HasUpdate error: " + error));
        }

        private void OpenInAppStore()
        {
            ErogamesAuth.OpenInAppStore("zh", (string error) => Debug.Log("On OpenInAppStore error: " + error));
        }
    }
}
