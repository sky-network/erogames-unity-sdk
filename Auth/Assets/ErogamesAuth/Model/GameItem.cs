﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class GameItem
    {
        public string app_id = null;
        public int versionCode = 0;
        public string rawVersionCode = null;
        public int siteVersionCode = 0;
        public string game_page = null;
    }
}
