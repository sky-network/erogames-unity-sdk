﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ErogamesAuthNS.Util;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class GameItemsResp
    {
        public static List<GameItem> ToGameItems(string json)
        {
            JSONNode jsonObj = JSON.Parse(json);
            JSONArray itemsNode = jsonObj["items"].AsArray;
            List<GameItem> gameItems = new List<GameItem>();

            foreach (JSONNode itemNode in itemsNode)
            {
                GameItem gameItem = JsonUtility.FromJson<GameItem>(itemNode.ToString());
                gameItems.Add(gameItem);
            }
            return gameItems;
        }
    }
}
