﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class Token
    {
        public string access_token;
        public string refresh_token;
        public string token_type;
        public int expires_in;
        public int created_at;

        public bool IsExpired()
        {
            long now = (long)DateTime.UtcNow.ToUniversalTime()
                .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                .TotalSeconds;
            return (now + 10) > created_at + expires_in;
        }
    }
}
