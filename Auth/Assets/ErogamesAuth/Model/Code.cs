﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    internal class Code
    {
        public string value;
        public string origin;

        public Code(string value, string origin)
        {
            this.value = value;
            this.origin = origin;
        }
    }
}
