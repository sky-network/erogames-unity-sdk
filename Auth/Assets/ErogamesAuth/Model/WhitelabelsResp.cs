﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ErogamesAuthNS.Util;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class WhitelabelsResp
    {
        public static List<Whitelabel> ToWhitelabels(string json)
        {
            JSONNode jsonObj = JSON.Parse(json);
            JSONArray whitelabelsNode = jsonObj["whitelabels"].AsArray;
            List<Whitelabel> whitelabels = new List<Whitelabel>();

            foreach (JSONNode whitelabelNode in whitelabelsNode)
            {
                StringStringDictionary erogoldUlrs = Utils.JSONNodeToDictionary(whitelabelNode["buy_erogold_url"]);
                StringStringDictionary supportUlrs = Utils.JSONNodeToDictionary(whitelabelNode["support_url"]);
                StringStringDictionary tosUlrs = Utils.JSONNodeToDictionary(whitelabelNode["tos_url"]);
                Whitelabel whitelabel = JsonUtility.FromJson<Whitelabel>(whitelabelNode.ToString());
                whitelabel.buy_erogold_url = erogoldUlrs;
                whitelabel.support_url = supportUlrs;
                whitelabel.tos_url = tosUlrs;
                whitelabels.Add(whitelabel);
            }
            return whitelabels;
        }

        public static Whitelabel ToWhitelabel(string json)
        {
            JSONNode jsonObj = JSON.Parse(json);

            StringStringDictionary erogoldUlrs = Utils.JSONNodeToDictionary(jsonObj["buy_erogold_url"]);
            StringStringDictionary supportUlrs = Utils.JSONNodeToDictionary(jsonObj["support_url"]);
            StringStringDictionary tosUlrs = Utils.JSONNodeToDictionary(jsonObj["tos_url"]);
            Whitelabel whitelabel = JsonUtility.FromJson<Whitelabel>(jsonObj.ToString());
            whitelabel.buy_erogold_url = erogoldUlrs;
            whitelabel.support_url = supportUlrs;
            whitelabel.tos_url = tosUlrs;
            return whitelabel;
        }
    }
}
