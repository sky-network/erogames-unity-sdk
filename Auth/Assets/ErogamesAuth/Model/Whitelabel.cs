﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class Whitelabel
    {
        public string slug;
        public string name;
        public string url;
        public string logo_url = null;
        public string dark_background_logo_url = null;
        public string dark_background_logo_svg_url = null;
        public string bright_background_logo_url = null;
        public string bright_background_logo_svg_url = null;
        public string authorize_url;
        public string token_url;
        public string profile_url;
        public StringStringDictionary buy_erogold_url;
        public StringStringDictionary support_url;
        public StringStringDictionary tos_url;
        public WLTheme theme = null;
        public string android_app_store_id = null;
        public string macos_app_store_id = null;
        public string win_app_store_id = null;
    }
}
