using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using System;
using UnityEngine;

namespace ErogamesAuthNS
{
    public class ErogamesAuth
    {
        private const string DefaultLang = "en";

        private ErogamesAuth() { }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void AutoLogin()
        {
            Init();
            if (GetUser() == null)
            {
                var withAutoLogin = ErogamesAuthBridge.Instance.WithAutoLogin();
                var isOnLogin = ErogamesAuthBridge.Instance.IsOnLogin();
#if UNITY_ANDROID || UNITY_STANDALONE
                if (withAutoLogin && !isOnLogin) Login("en");
#endif
#if UNITY_WEBGL
                string windowName = WebGLAuthFlow.GetWindowName();
                if (windowName == "com_erogames_sdk_auth_iframe") return;
                if (withAutoLogin || isOnLogin)
                {
                    string code = Utils.ParseQueryString(Application.absoluteURL, "code");
                    if (code != null)
                    {
                        string baseUrl = Utils.RemoveQueryString(Application.absoluteURL, "code");
                        WebGLAuthFlow.ReplaceHistoryState(baseUrl);
                        ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.ProceedAuth(code));
                    }
                    else
                    {
                        Login("en");
                    }
                }
#endif
            }
            else
            {
                ErogamesAuthBridge.Instance.OnAuthSuccess();
            }
        }

        private static void Init()
        {
            ErogamesAuthBridge.Instance.Init();
        }

        /// <summary>
        /// Monitors the authentication state.
        /// </summary>
        /// <param name="onAuthCallback">
        /// will trigger once the user has logged in, signed up, or logged out.
        /// User != null and Error == null: A user is logged in/signed up,
        /// User == null and Error != null: A user is NOT logged in/signed up/logged out,
        /// User == null and Error == null: A user is logged out,
        /// </param>
        public static void OnAuth(OnAuthCallback callback)
        {
            ErogamesAuthBridge.Instance.SetAuthCallback(callback);
        }

        /// <summary>
        /// Starts sign in process. The corresponding "sign in" web URL will be opened in the default browser.
        /// The Custom Chrome Tabs can be used if available.
        /// </summary>
        /// <param name="language">
        /// The preferable language of the "sign in" web page.
        /// </param>
        public static void Login(string language)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.Login(language ?? DefaultLang));
        }


        /// <summary>
        /// Starts sign in process. The corresponding "sign up" web URL will be opened in the default browser.
        /// The Custom Chrome Tabs can be used if available.
        /// </summary>
        /// <param name="language">
        /// The preferable language of the "sign up" web page.
        /// </param>
        public static void Signup(string language)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.Signup(language ?? DefaultLang));
        }


        /// <summary>
        /// Clear all local authentication data.
        /// </summary>
        /// <param name="webLogout">
        /// For Android only, if [webLogout] is true then the corresponding "log out" web URL will be opened in the default browser.
        /// In this case, the user will also be logged out from the web site.
        /// </param>
        public static void Logout(bool webLogout = false)
        {
            ErogamesAuthBridge.Instance.Logout(webLogout);
        }

        /// <summary>
        /// Returns the current user data, or null if the user is not logged in.
        /// </summary>
        /// <returns>User</returns>
        public static User GetUser()
        {
            return ErogamesAuthBridge.Instance.GetUser();
        }

        /// <summary>
        /// Refreshes the current user data.
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void ReloadUser(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.ReloadUser(onSuccess, onFailure));
        }

        /// <summary>
        /// Returns the current token, or null if the user is not logged in.
        /// </summary>
        /// <returns>Token</returns>
        public static Token GetToken()
        {
            return ErogamesAuthBridge.Instance.GetToken();
        }

        /// <summary>
        /// Refreshes the current token. For example, can be used if the token is expired.
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void RefreshToken(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.RefreshToken(onSuccess, onFailure));
        }

        /// <summary>
        /// Proceed a new payment.
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="amount">the amount in Erogold</param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void ProceedPayment(string paymentId, int amount, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.ProceedPayment(paymentId, amount, onSuccess, onFailure));
        }


        /// <summary>
        /// Returns a whitelabel info.
        /// </summary>
        /// <returns>Whitelabel</returns>
        public static Whitelabel GetWhitelabel()
        {
            return ErogamesAuthBridge.Instance.GetWhitelabel();
        }

        /// <summary>
        /// Load the whitelabel. For example, can be used if the user is not logged in.
        /// To get already loaded whitelabel <see cref="GetWhitelabel"/>
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadWhitelabel(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadWhitelabel(onSuccess, onFailure));
        }

        /// <summary>
        /// Loads current quest info.
        ///
        /// Load key quest statistics and fields such as quest title and
        /// description.The data returned also has information about the current ranking of
        /// user's clan (user is loaded based on used auth token) and also current user's
        /// individual contribution to the clan score.This endpoint allows implementing in-game
        /// quest status widgets for more immersive gameplay.
        /// 
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadCurrentQuest(OnResult<QuestData> onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadCurrentQuest(onSuccess, onFailure));
        }

        /// <summary>
        /// Loads payment information
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadPaymentInfo(string paymentId, OnResult<PaymentInfo> onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadPaymentInfo(paymentId, onSuccess, onFailure));
        }

        public static bool IsLogging()
        {
            return ErogamesAuthBridge.Instance.IsOnLogin();
        }

        [Obsolete("Deprecated. Use HasUpdate(string locale, OnResult<bool> onSuccess, OnResultFailureDelegate onFailure) instead.")]
        public static void HasUpdate(OnResult<bool> onSuccess, OnResultFailureDelegate onFailure)
        {
            HasUpdate("en", onSuccess, onFailure);
        }

        public static void HasUpdate(string locale, OnResult<bool> onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.HasUpdate(locale, onSuccess, onFailure));
        }

        [Obsolete("Deprecated. Use OpenInAppStore(string locale, OnResult<string> onFailure) instead.")]
        public static void OpenInAppStore(OnResult<bool> onSuccess, OnResultFailureDelegate onFailure)
        {
            OpenInAppStore("en", (string error) => onFailure(error));
        }

        public static void OpenInAppStore(string locale, OnResult<string> onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.OpenInAppStore(locale, onFailure));
        }
    }
}
