﻿#if UNITY_STANDALONE
using System;
using System.Collections;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.UI;
using ErogamesAuthNS.Util;
using ErogamesAuthNS.Repository;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Net;
using System.ComponentModel;
using System.Net.Sockets;

namespace ErogamesAuthNS.Standalone
{
    internal class StandaloneAuthFlow : BaseAuthFlow
    {
        private string RedirectUri = "";
        private const string VerifierKey = "com_erogames_sdk_unity_code_verifier_key";
        private readonly string ResponseString = "<html><head><script type='text/javascript'>function closeWindow() { window.close(); } setTimeout(closeWindow, 0);</script></head><body></body></html>";
        private readonly BackgroundWorker HttpContextWorker = new BackgroundWorker();
        private readonly BackgroundWorker StreamWorker = new BackgroundWorker();

        internal StandaloneAuthFlow(BaseRepository repository, Action<Result<bool>> authResult) : base(repository, authResult) { }


#if UNITY_STANDALONE_WIN
        private IntPtr unityWindow;
        const int ALT = 0xA4;
        const int EXTENDEDKEY = 0x1;
        const int KEYUP = 0x2;

        [DllImport("user32.dll")]
        internal static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll")]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        internal static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);
#endif


        private void ForegroundWindow()
        {
#if UNITY_STANDALONE_OSX
            ErogamesAuthPlugin.ActivateWindow();
#elif UNITY_STANDALONE_WIN
            // Simulate alt press
            keybd_event((byte)ALT, 0x45, EXTENDEDKEY | 0, 0);
            // Simulate alt release
            keybd_event((byte)ALT, 0x45, EXTENDEDKEY | KEYUP, 0);
            if (unityWindow != null)
            {
                SetForegroundWindow(unityWindow);
                ShowWindow(unityWindow, 10);
            }
#endif
        }

        private static int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }

        internal override IEnumerator StartAuth(string lang, bool signup = false)
        {
#if UNITY_STANDALONE_WIN
            unityWindow = GetActiveWindow();
#endif

            RedirectUri = string.Format("http://{0}:{1}/", "localhost", GetRandomUnusedPort());
            Debug.Log("redirectUri: " + RedirectUri);

            string authUrl = null;
            yield return base.BuildAuthUrl(lang, (result) => { authUrl = result; }, signup);

            var http = new HttpListener();
            http.Prefixes.Add(string.Format("http://+:{0}/", new Uri(RedirectUri).Port));
            Debug.Log("Start HTTP server...");
            http.Start();

            Debug.Log("Open auth URL...");
            //System.Diagnostics.Process.Start(authUrl);
            Application.OpenURL(authUrl);

            // Get HTTP request.
            HttpListenerContext context = null;
            Action contextAction = () =>
            {
                context = http.GetContext();
                ForegroundWindow();
            };
            Action<string> contextError = (error) => { Debug.Log(error); };
            yield return Utils.DoWork(contextAction, contextError, HttpContextWorker);

            // Sends an HTTP response to the browser.
            var response = context.Response;
            var buffer = System.Text.Encoding.UTF8.GetBytes(ResponseString);
            response.ContentLength64 = buffer.Length;
            var outputStream = response.OutputStream;

            Action writeAction = () => {outputStream.Write(buffer, 0, buffer.Length);};
            Action<string> writeError = (error) => { Debug.Log(error); };
            yield return Utils.DoWork(writeAction, writeError, StreamWorker);

            outputStream.Close();
            http.Stop();
            Debug.Log("HTTP server stopped.");

            var code = context.Request.QueryString.Get("code");
            yield return ProceedAuth(code);
        }


        internal override string createCodeVerifier()
        {
            string codeVerifier = Pkce.Instance.GetCodeVerifier();
            PlayerPrefs.SetString(VerifierKey, codeVerifier);
            return codeVerifier;
        }

        internal override string getCodeVerifier(bool postRemove = true)
        {
            string codeVerifier = PlayerPrefs.GetString(VerifierKey);
            if (postRemove) PlayerPrefs.DeleteKey(VerifierKey);
            return codeVerifier;
        }

        internal override string getRedirectUri(bool encoded)
        {
            if (encoded) return Uri.EscapeDataString(RedirectUri);
            return RedirectUri;
        }
    }
}
#endif