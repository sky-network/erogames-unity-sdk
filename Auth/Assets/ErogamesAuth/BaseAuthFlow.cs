﻿using System;
using System.Collections;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using ErogamesAuthNS.Repository;

namespace ErogamesAuthNS
{
    internal abstract class BaseAuthFlow
    {
        private readonly BaseRepository repository;
        private readonly Action<Result<bool>> authResult;

        internal BaseAuthFlow(BaseRepository repository, Action<Result<bool>> authResult)
        {
            this.repository = repository;
            this.authResult = authResult;
        }

        protected IEnumerator BuildAuthUrl(string lang, Action<string> onAuthUrl, bool signup = false)
        {
            Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
            yield return repository.LoadWhitelabel((result) => { whitelabelResult = result; });
            if (whitelabelResult is Result<Whitelabel>.ErrorResult)
            {
                authResult(Result<bool>.Error(whitelabelResult.ErrorMsg));
                yield break;
            }

            string codeChallenge = Pkce.Instance.GenerateCodeChallenge(createCodeVerifier());
            string authorizeUrl = whitelabelResult.Data.authorize_url;
            string clientId = repository.GetClientId();
            string authUrl = AuthUtil.BuildAuthUrl(authorizeUrl, codeChallenge, getRedirectUri(true), clientId, lang, signup);
            onAuthUrl(authUrl);
        }
 
        internal IEnumerator ProceedAuth(string code)
        {
            var tokenResp = Result<Token>.Undefined();
            var userResp = Result<User>.Undefined();

            yield return repository.LoadToken(code, getRedirectUri(false), getCodeVerifier(), (t) => { tokenResp = t; });
            if (tokenResp is Result<Token>.ErrorResult)
            {
                authResult(Result<bool>.Error(tokenResp.ErrorMsg));
                yield break;
            }

            yield return repository.LoadUser((u) => { userResp = u; });
            if (userResp is Result<User>.ErrorResult)
            {
                authResult(Result<bool>.Error(userResp.ErrorMsg));
                yield break;
            }
            authResult(Result<bool>.Success(true));
        }

        internal virtual void Logout(bool webLogout)
        {
            repository.ClearData();
        }

        internal string GetLogoutUrl()
        {
            Whitelabel wl = repository.GetWhitelabel();
            Token token = repository.GetToken();
            return AuthUtil.BuildWebLogoutUrl(wl.url, token.access_token, getRedirectUri(true));
        }

        abstract internal IEnumerator StartAuth(string lang, bool signup = false);
        abstract internal string createCodeVerifier();
        abstract internal string getCodeVerifier(bool postRemove = true);
        abstract internal string getRedirectUri(bool encoded);
    }
}
