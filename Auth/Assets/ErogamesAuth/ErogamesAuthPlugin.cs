using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ErogamesAuthNS
{
    internal class ErogamesAuthPlugin
    {


#if UNITY_STANDALONE_OSX
        [DllImport("ErogamesAuthPlugin")]
        internal static extern void ActivateWindow();

        [DllImport("ErogamesAuthPlugin")]
        internal static extern bool CanOpenURL(string url);
#endif

#if UNITY_STANDALONE_WIN
        [DllImport("ErogamesAuthPlugin")]
        internal static extern bool CanOpenURL(string url);
#endif

#if UNITY_ANDROID
        internal static bool CanOpenURL(string url)
        {
            try
            {
                string packageName = new Uri(url).Scheme;
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                var pm = currentActivity.Call<AndroidJavaObject>("getPackageManager");
                var packageInfo = pm.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError("CanOpenURL: " + e.StackTrace);
                return false;
            }
        }

        internal static void StartActivity(string url)
        {
            try
            {
                AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", "android.intent.action.VIEW");
                AndroidJavaClass Uri = new AndroidJavaClass("android.net.Uri");
                AndroidJavaObject uri = Uri.CallStatic<AndroidJavaObject>("parse", url);
                intent.Call<AndroidJavaObject>("setData", uri);

                int f1 = intent.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
                int f2 = intent.GetStatic<int>("FLAG_ACTIVITY_CLEAR_TASK");

                intent.Call<AndroidJavaObject>("addFlags", f1);
                intent.Call<AndroidJavaObject>("addFlags", f2);

                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

                currentActivity.Call("startActivity", intent);
            }
            catch (Exception e)
            {
                Debug.LogError("StartActivity: " + e.StackTrace);
            }
        }
#endif
    }
}
