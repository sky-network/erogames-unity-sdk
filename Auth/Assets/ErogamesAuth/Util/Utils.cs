﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using ErogamesAuthNS.Model;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAuthNS.Util
{
    public class Utils
    {
        public static StringStringDictionary JSONNodeToDictionary(JSONNode jsonNode)
        {
            StringStringDictionary dict = new StringStringDictionary();
            foreach (string key in jsonNode.Keys)
            {
                dict.Add(key, jsonNode[key]);
            }

            return dict;
        }

        public static string ParseQueryString(string url, string key)
        {
            Uri appUri;
            try
            {
                appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return null;
            }

            string query = appUri.Query;
            query = query.Replace("?", "");
            if (query == null || query.Length < 1 || !query.Contains("=")) return null;
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );

            return keysValues.ContainsKey(key) ? keysValues[key] : null;
        }

        public static string RemoveQueryString(string url, string key)
        {
            if (string.IsNullOrEmpty(url)) return url;
            if (!url.Contains("?")) return url;
            if (!IsUrlValid(url)) return url;

            string left = url.Substring(0, url.IndexOf("?"));
            string query = url.Substring(url.IndexOf("?")).Remove(0, 1);

            if (query == null || query.Length < 1 || !query.Contains("=")) return url;
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );
            keysValues.Remove(key);

            string newQuery = "";
            foreach (string k in keysValues.Keys)
            {
                newQuery += "&" + k + "=" + keysValues[k];
            }

            if (newQuery.Length > 0) newQuery = "?" + newQuery.Remove(0, 1);
            return left + newQuery;
        }

        internal static bool IsUrlValid(string url)
        {
            try
            {
                Uri appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return false;
            }
            return true;
        }

        public static bool IsResponseSuccessful(UnityWebRequest unityWebRequest)
        {
            return unityWebRequest.responseCode >= 200 && unityWebRequest.responseCode <= 299;
        }


        internal static IEnumerator DoWork(Action action, Action<string> onError, BackgroundWorker worker = null)
        {
            var worker1 = worker;
            if (worker1 == null) worker1 = new BackgroundWorker();
            bool completed = false;

            DoWorkEventHandler doWorkHandler = (sender, args) =>
            {
                Debug.Log("DoWork: " + worker1.GetHashCode());
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    onError(e.Message);
                }
                completed = true;
            };

            worker1.DoWork += doWorkHandler;
            worker1.RunWorkerAsync();
            yield return new WaitUntil(() =>
            {
                if (worker1.CancellationPending)
                {
                    onError("Cancelled");
                    return true;
                }
                return completed;
            });

            worker1.DoWork -= doWorkHandler;
        }

        internal static string ReadChars(string path, int count)
        {
            string resultString = null;
            if (!File.Exists(path))
            {
                Debug.Log("File " + path + " does not exist.");
                return null;
            }
            char[] buffer = new char[count];
            using (StreamReader reader = new StreamReader(path))
            {
                int n = reader.ReadBlock(buffer, 0, buffer.Length);
                char[] result = new char[n];
                Array.Copy(buffer, result, n);
                resultString = new string(result);
            }
            return resultString;
        }

        internal static string GetPlatformName()
        {
            if (Application.platform == RuntimePlatform.Android) return "android";
            if (Application.platform == RuntimePlatform.OSXPlayer) return "osx";
            if (Application.platform == RuntimePlatform.WindowsPlayer) return "windows";
            if (Application.platform == RuntimePlatform.LinuxPlayer) return "linux";
            if (Application.platform == RuntimePlatform.WebGLPlayer) return "web";
            return "";
        }
    }
}
