﻿using System;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class AuthUtil
    {
        internal static readonly string DeepLinkFormat = "{0}://autoauth";
        internal static readonly string ErogoldUrlFormat = "{0}/{1}/buy-erogold";
        internal static readonly string WebLogoutUrlFormat = "{0}/logout?token={1}&redirect_uri={2}";
        internal static readonly string AuthUrlFormat = "{0}{1}" +
            "code_challenge={2}" +
            "&redirect_uri={3}" +
            "&client_id={4}" +
            "&locale={5}" +
            "&force_registration={6}" +
            "&code_challenge_method=S256" +
            "&response_type=code" +
            "&disclaimer=none";

        private AuthUtil() { }

        internal static string BuildAuthUrl(
            string authorizeUrl,
            string codeChallenge,
            string redirectUri,
            string clientId,
            string lang,
            bool forceRegistration
            )
        {
            return string.Format(AuthUrlFormat,
             authorizeUrl,
             authorizeUrl.Contains("?") ? "&" : "?",
             codeChallenge,
             redirectUri,
             clientId,
             lang,
             forceRegistration.ToString().ToLower());
        }

        internal static string BuildRedirectUri()
        {
            return BuildRedirectUri(false);
        }

        internal static string BuildRedirectUri(bool encode)
        {
#if UNITY_STANDALONE_OSX
            string url = string.Format(DeepLinkFormat, Application.identifier);
            Debug.Log("Redirect URL: " + url);
            if (encode) return Uri.EscapeDataString(url);
            return url;
#elif UNITY_WEBGL
            string url = Application.absoluteURL;
            url = Utils.RemoveQueryString(url, "code");

            if (encode)
            {
                return Uri.EscapeDataString(url);
            }
            Debug.Log("Redirect URL: " + url);
            return url;
#else
            return null;
#endif
        }

        internal static string BuildWebLogoutUrl(string baseUrl, string token, string redirectUri)
        {
            return string.Format(WebLogoutUrlFormat, baseUrl, token, redirectUri);
        }
    }
}
