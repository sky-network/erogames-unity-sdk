﻿using System.IO;
using System.Xml.Serialization;
using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class ApiPrefsUtil
    {
        private const string ApiPrefsXmlFile = "ErogamesAuthApiPrefs";
        private static readonly string WhitelabelIdFilename = "erogames_whitelabel_id";

        private ApiPrefsUtil() { }

        internal static ApiPrefs LoadApiPrefs()
        {
            string fileText = Resources.Load<TextAsset>(ApiPrefsXmlFile).text;
            XmlSerializer serializer = new XmlSerializer(typeof(ApiPrefs));
#pragma warning disable IDE0063 // Use simple 'using' statement
            using (StringReader reader = new StringReader(fileText))
#pragma warning restore IDE0063 // Use simple 'using' statement
            {
                return (ApiPrefs)serializer.Deserialize(reader);
            }
        }

        internal static ApiPrefs LoadMergedApiPrefs()
        {
            ApiPrefs apiPrefs = LoadApiPrefs();
            ApiPrefsScriptableObject apiPrefsScriptableObject = ApiPrefsScriptableObject.Load();

            if (apiPrefsScriptableObject != null)
            {
                apiPrefs.whitelabelId = string.IsNullOrEmpty(apiPrefsScriptableObject.whitelabelId)
                    ? apiPrefs.whitelabelId : apiPrefsScriptableObject.whitelabelId;
                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    string whitelabelId = Utils.ParseQueryString(Application.absoluteURL, "whitelabel");
                    apiPrefs.whitelabelId = whitelabelId;
                } else if (Application.platform == RuntimePlatform.OSXPlayer ||
                    Application.platform == RuntimePlatform.WindowsPlayer ||
                    Application.platform == RuntimePlatform.LinuxPlayer)
                {
                    string path = Application.dataPath;
                    path = Path.Combine(path, WhitelabelIdFilename);
                    var whitelabelIdFromFile = Utils.ReadChars(path, 15);
                    if (!string.IsNullOrEmpty(whitelabelIdFromFile)) apiPrefs.whitelabelId = whitelabelIdFromFile;
                }
                if(string.IsNullOrEmpty(apiPrefs.whitelabelId)) apiPrefs.whitelabelId = "erogames";

                apiPrefs.clientId = string.IsNullOrEmpty(apiPrefsScriptableObject.clientId)
                    ? apiPrefs.clientId : apiPrefsScriptableObject.clientId;
                apiPrefs.autoLogin = apiPrefsScriptableObject.autoLogin;
                apiPrefs.accessKey = string.IsNullOrEmpty(apiPrefsScriptableObject.accessKey)
                    ? apiPrefs.accessKey : apiPrefsScriptableObject.accessKey;
            }
            return apiPrefs;
        }
    }
}