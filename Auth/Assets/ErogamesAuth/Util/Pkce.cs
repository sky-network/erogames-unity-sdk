﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ErogamesAuthNS.Util
{
    internal class Pkce : Singleton<Pkce>
    {
        private string CodeVerifier = null;

        internal string GetCodeVerifier(bool generateNew = false)
        {
            if (CodeVerifier == null || generateNew)
            {
                CodeVerifier = GenerateCodeVerifier();
            }
            return CodeVerifier;
        }

        private string GenerateCodeVerifier()
        {
            byte[] bytes = new byte[32];
            RandomNumberGenerator.Create().GetBytes(bytes);
            return Convert.ToBase64String(bytes).TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }

        internal string GenerateCodeChallenge(string codeVerifier)
        {
            using (var sha256 = SHA256.Create())
            {
                byte[] challengeBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(codeVerifier));
                string codeChallenge = Convert.ToBase64String(challengeBytes)
                    .TrimEnd('=')
                    .Replace('+', '-')
                    .Replace('/', '_');
                return codeChallenge;
            }
        }
    }
}
