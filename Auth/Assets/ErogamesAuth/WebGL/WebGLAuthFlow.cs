#if UNITY_WEBGL
using System;
using System.Collections;
using System.Runtime.InteropServices;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Repository;
using ErogamesAuthNS.Util;
using UnityEngine;

namespace ErogamesAuthNS
{
    internal class WebGLAuthFlow : BaseAuthFlow
    {
        internal WebGLAuthFlow(BaseRepository repository, Action<Result<bool>> authResult) : base(repository,
            authResult)
        {
        }

        [DllImport("__Internal")]
        private static extern void TakeCodeFromIframe(string authUrl);

        [DllImport("__Internal")]
        private static extern string GetWindowNameInternal();

        [DllImport("__Internal")]
        private static extern void ReplaceHistoryStateInternal(string url);

        [DllImport("__Internal")]
        private static extern void WindowLocationReplaceInternal(string url);

        [DllImport("__Internal")]
        private static extern void WebLogoutInternal(string url);

        [DllImport("__Internal")]
        private static extern string GetItem(string key);

        [DllImport("__Internal")]
        private static extern void SetItem(string key, string value);

        [DllImport("__Internal")]
        private static extern void RemoveItem(string key);


        internal override IEnumerator StartAuth(string lang, bool signup = false)
        {
            yield return BuildAuthUrl(lang, authUrl => { TakeCodeFromIframe(authUrl); }, signup);
        }


        internal static void ReplaceHistoryState(string url)
        {
            ReplaceHistoryStateInternal(url);
        }

        internal static void WindowLocationReplace(string url)
        {
            WindowLocationReplaceInternal(url);
        }

        internal static string GetWindowName()
        {
            return GetWindowNameInternal();
        }

        override internal void Logout(bool webLogout)
        {
            string logoutUrl = GetLogoutUrl();
            base.Logout(webLogout);
            if (webLogout) WebLogoutInternal(logoutUrl);
        }

        internal override string createCodeVerifier()
        {
            string codeVerifier = Pkce.Instance.GetCodeVerifier();
            SetItem("com_erogames_auth_codeverifier", codeVerifier);
            return codeVerifier;
        }

        internal override string getCodeVerifier(bool postRemove = true)
        {
            string codeVerifier = "";
            codeVerifier = GetItem("com_erogames_auth_codeverifier");
            if (postRemove) RemoveItem("com_erogames_auth_codeverifier");
            return codeVerifier;
        }

        internal override string getRedirectUri(bool encoded)
        {
            string url = Application.absoluteURL;
            url = Utils.RemoveQueryString(url, "code");

            if (encoded) return Uri.EscapeDataString(url);
            Debug.Log("Redirect URL: " + url);
            return url;
        }
    }
}
#endif