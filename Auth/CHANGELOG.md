# Version 2.0.9 - Jul 13, 2023
* Fixed checking for game updates.

# Version 2.0.8 - May 8, 2023
* Fixed the opening of the game page in the browser if the app store is not installed.

# Version 2.0.7 - May 2, 2023
* Bug fixes.

# Version 2.0.6 - May 2, 2023
* Fixed a lot of bugs.

# Version 2.0.5 - Apr 23, 2023
* Fixed opening the browser after launching the app store (Windows, macOS).

# Version 2.0.4 - Dec 14, 2022
* Bug fixes.

# Version 2.0.3 - Dec 13, 2022
* Added the ability to check for game updates.
* Added the ability to show the game in the app store (if not available, open the game web page).

# Version 2.0.2 - Nov 30, 2022
* Added Whitelabel theme.

# Version 2.0.1 - Nov 18, 2022
* Fixed macOS (Catalina) plugin issue.

# Version 2.0.0 - Nov 10, 2022
* Added full support for Windows and macOS.
* Removed 'RegisterUser()' method. Instead, 'Signup()' should be used.
* Removed 'LoginByPassword()' method. Instead, 'Login()' should be used.
* Fixed a few minor issues.

# Version 1.4.18 - Dec 18, 2021
* Refresh token automatically if it expired.

# Version 1.4.17 - Jun 11, 2021
* Bug fixes and stability improvements.

# Version 1.4.16 - Jun 11, 2021
* Bug fixes and stability improvements.

# Version 1.4.15 - Jun 04, 2021
* Disabled an ability to edit API base URL.
* Added a whitelabel feature for 'register a new user'.
* Bug fixes.

# Version 1.4.14 - Jun 02, 2021
* Update README.

# Version 1.4.13 - May 31, 2021
* Minor update.

# Version 1.4.12 - May 25, 2021
* Minor update.

# Version 1.4.11 - May 25, 2021
## Removed
* Game Package Registry by Google and related dependencies.

# Version 1.4.10 - May 14, 2021
* Minor update.

# Version 1.4.9 - May 14, 2021
* Minor update.

# Version 1.4.8 - May 11, 2021
## Fixed
* Crashes in incognito mode.

# Version 1.4.7 - Apr 25, 2021
## Added
* Ability to get payment information.

# Version 1.4.6 - Apr 21, 2021
## Added
* PKCE protection.

# Version 1.4.5 - Apr 07, 2021
## Added
* Add auth callback call (ErogamesAurh.OnAuth(OnAuthCallback callback)) in case the user canceled the authentication.

# Version 1.4.4 - Apr 07, 2021
## Fixed
* Deprecation warnings on Unity 2020.

# Version 1.4.3 - Apr 01, 2021
## Fixed
* Inability to assign a default whitelabel id on WebGL.
* Inability to change whitelabel id on Android build.
* C# 4.0 compatibility.
## Added
* New fields to QuestData: 'best_players' and 'user_attempt'.
